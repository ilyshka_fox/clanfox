package ru.ilyshka_fox.clanfox;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import ru.ilyshka_fox.clanfox.core.menu.GUIController;
import ru.ilyshka_fox.clanfox.core.menu.anvil.AnvilHolder;
import ru.ilyshka_fox.clanfox.core.sql.QuerySQL;
import ru.ilyshka_fox.clanfox.core.sql.SQL;
import ru.ilyshka_fox.clanfox.core.yamController.yamController;
import ru.ilyshka_fox.clanfox.events.CmdClan;
import ru.ilyshka_fox.clanfox.events.Join;
import ru.ilyshka_fox.clanfox.events.chat.Chat;
import ru.ilyshka_fox.clanfox.events.kill;
import ru.ilyshka_fox.clanfox.setting.Config;
import ru.ilyshka_fox.clanfox.setting.Icons;


public class Main extends JavaPlugin implements Listener {
    public static boolean update = false;

    public static Plugin plugin;
    public static Economy econ = null;
    public static SQL sql = new SQL();
    public static QuerySQL QSQL = new QuerySQL();


    public void onEnable() {

        plugin = this;

        // TODO инициализация плагинов и предзагрузка адонов

        // одключение эвентов и коменд
        getServer().getPluginManager().registerEvents(new GUIController(), Main.plugin);
        getServer().getPluginManager().registerEvents(new Join(), Main.plugin);
        getServer().getPluginManager().registerEvents(new AnvilHolder(), Main.plugin);
        getServer().getPluginManager().registerEvents(new Chat(), Main.plugin);
        getServer().getPluginManager().registerEvents(new kill(), Main.plugin);

        getCommand("clanfox").setExecutor(new CmdClan());

        // Загрузка списка иконок
        Icons.LoadConfig();


        // проверка экономики
        if (Config.vault && !setupEconomy()) {
            getLogger().warning("No Vault dependency found!");
            Config.vault = false;
        }


        // подключение к бд
        sql.connect();


        // TODO пост загрузка адонов
        sendMSGColor("");
        sendMSGColor("=======================================================");
        sendMSGColor("");
        sendMSGColor(ChatColor.GOLD + "              2Xr                     .rX2      ");
        sendMSGColor(ChatColor.GOLD + "              X999i.                 s993X      ");
        sendMSGColor(ChatColor.GOLD + "              593399s              s99999S      ");
        sendMSGColor(ChatColor.GOLD + "              ;9X99399r          r993933hr      ");
        sendMSGColor(ChatColor.GOLD + "               G," + ChatColor.GRAY + ":" + ChatColor.GOLD
                + "S99393;      ;X9399S" + ChatColor.GRAY + ":," + ChatColor.GOLD + "G.      ");
        sendMSGColor(ChatColor.GOLD + "               XS" + ChatColor.GRAY + ",,," + ChatColor.GOLD + "i932S.    .S239i"
                + ChatColor.GRAY + ",,," + ChatColor.GOLD + "iX       ");
        sendMSGColor(ChatColor.GOLD + "               ;G" + ChatColor.GRAY + ",.,...          .,.,,," + ChatColor.GOLD
                + "hr       ");
        sendMSGColor(ChatColor.GOLD + "                99" + ChatColor.GRAY + ",                  ," + ChatColor.GOLD
                + "3h        ");
        sendMSGColor(ChatColor.GOLD + "                2X                    X2        ");
        sendMSGColor(ChatColor.GOLD + "                :                      :        ");
        sendMSGColor("");
        sendMSGColor(" $$$$   $$       $$$$   $$  $$  $$$$$$   $$$$   $$  $$ ");
        sendMSGColor("$$  $$  $$      $$  $$  $$$ $$  $$      $$  $$   $$$$  ");
        sendMSGColor("$$      $$      $$$$$$  $$ $$$  $$$$    $$  $$    $$   ");
        sendMSGColor("$$  $$  $$      $$  $$  $$  $$  $$      $$  $$   $$$$  ");
        sendMSGColor(" $$$$   $$$$$$  $$  $$  $$  $$  $$       $$$$   $$  $$ ");
        sendMSGColor("");
        sendMSGColor("                    by ilyshka_fox");
        sendMSGColor("=======================================================");
        sendMSGColor("               http://vk.com/ilyshkafox");

        Bukkit.getScheduler().runTaskAsynchronously(this, () -> {
            yamController.init();
            Bukkit.getOnlinePlayers().forEach(Join::updateAccaunt);
            Config.vault = Config.vault && econ != null;
            CmdClan.load = true;
        });


    }

    public static void sendMSGColor(String s) {
        Bukkit.getConsoleSender().sendMessage("[ClanFox] " + s);
    }

    public void onDisable() {
        // API.getReg().forEach(x -> API.unRegisterAddons(x));
        Data.clear();
        Chat.close();
        AnvilHolder.closeAll();
        GUIController.closeAll();
        plugin = null;
        econ = null;
        sql = null;
        getLogger().info("<<==OFF=ClanFox=by=ilyshkaFox==>>");
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

}
