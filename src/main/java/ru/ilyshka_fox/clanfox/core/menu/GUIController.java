package ru.ilyshka_fox.clanfox.core.menu;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import ru.ilyshka_fox.clanfox.menus.MenuEx;

import java.util.ArrayList;

public class GUIController implements Listener {

    private static ArrayList<Player> openinv = new ArrayList<>();

    public static synchronized void addOpen(Player p) {
        if (!openinv.contains(p)) {
            openinv.add(p);
        }
    }

    public static synchronized void remOpen(Player p) {
        if (openinv.contains(p)) {
            openinv.remove(p);
        }
    }

    public static void closeAll() {
        new ArrayList<>(openinv).forEach(Player::closeInventory);
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        Inventory inv = e.getInventory();
        if (inv != null) {
            if (inv.getType() == InventoryType.CHEST && inv.getHolder() instanceof GUIHolder) {
                GUIHolder g = (GUIHolder) inv.getHolder();
                g.destroy();
            }
        }
        remOpen((Player) e.getPlayer());
    }


    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        Inventory clickedInventory;
        int slot = e.getRawSlot();
        if (slot < 0) {
            clickedInventory = null;
        } else if (e.getView().getTopInventory() != null && slot < e.getView().getTopInventory().getSize()) {
            clickedInventory = e.getView().getTopInventory();
        } else {
            clickedInventory = e.getView().getBottomInventory();
        }

        if (clickedInventory != null) {
            if (clickedInventory.getType() == InventoryType.CHEST && clickedInventory.getHolder() instanceof GUIHolder) {
                e.setCancelled(true);
                ((Player) e.getWhoClicked()).updateInventory();
                if (e.getCurrentItem() == null)
                    return;
                GUIHolder g = (GUIHolder) clickedInventory.getHolder();
                g.run((Player) e.getWhoClicked(), e.getCurrentItem(), e.getClick());
            } else if (e.getAction() == InventoryAction.COLLECT_TO_CURSOR || e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY)
                if (e.getInventory().getType() == InventoryType.CHEST
                        && e.getInventory().getHolder() instanceof GUIHolder) {
                    e.setCancelled(true);
                }
        }
    }

}
