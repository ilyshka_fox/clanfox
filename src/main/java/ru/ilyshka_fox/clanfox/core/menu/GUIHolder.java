package ru.ilyshka_fox.clanfox.core.menu;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import ru.ilyshka_fox.clanfox.Main;
import ru.ilyshka_fox.clanfox.core.yamController.global.constructor;
import ru.ilyshka_fox.clanfox.menus.MenuEx;

import java.util.ArrayList;
import java.util.HashMap;

public class GUIHolder implements InventoryHolder {

    private static boolean old = false;


    static {
        try {
            Bukkit.createInventory(null, 9, "12345678901234567890123456789012345678901234567890");
        } catch (Exception e) {
            Main.plugin.getLogger().info("Title Inventory constraints Mode is enabled");
            old = true;
        }
    }

    private int line = 6;
    private String name = ChatColor.RED + "No Name";
    private Item_builder[] content = new Item_builder[54];
    private Inventory inv;

    public GUIHolder() {
    }

    public GUIHolder(int line) {
        this.line = line < 1 ? 1 : (line > 6 ? 6 : line);
    }

    public GUIHolder(String name) {
        setTitle(name);
    }

    public GUIHolder(String name, int line) {
        this.line = line;
        setTitle(name);
    }

    public GUIHolder setButton(int pos, Item_builder b) {
        if (pos >= 0 && pos < 54) {
            content[pos] = b;
        }
        return this;
    }

    public GUIHolder setTitle(String name) {
        this.name = ChatColor.translateAlternateColorCodes('&', name);
        return this;
    }

    public GUIHolder addButton(Item_builder b) {
        for (int i = 0; i < 54; i++) {
            if (content[i] == null) {
                content[i] = b;
                break;
            }
        }
        return this;
    }

    HashMap<ItemStack, Item_builder> ItoB = new HashMap<>();

    public void run(Player p, ItemStack currentItem, ClickType action) {
        if (ItoB.containsKey(currentItem) && ItoB.get(currentItem).getRun() != null) {
            try {
                p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1.0f, 1.0f);
            } catch (Exception e2) {
                //Если игроку не проиграеться музыка по какимто причинам. то мне пофигу на это  =)
            }

            ItoB.get(currentItem).getRun().onInteract(currentItem, action);
        }
    }

    public void open(Player p) {
        open(p, true);
    }

    public void open(Player p, boolean check) {
        MenuEx.runCuns(() -> {
            InventoryHolder ph = p.getOpenInventory() != null ? p.getOpenInventory().getTopInventory() != null ? p.getOpenInventory().getTopInventory().getHolder() : null : null;
            if (!check || (ph != null && (ph instanceof GUIHolder))) {


                // Фон нужен для того чтобы не словать иныентарь.  Очень много разных эвентом. и мне просто лень изчать все.
                // легче просто заблокировать верхнюю часть и проверять на взаимодествие. то размазать предметы по обоим
                // инвентарям не получиться.
                // и нми ктоне будет заловаться чтоплагин забирает предметы
                Item_builder fonPanel = new Item_builder(constructor.fon).name(ChatColor.RESET.toString()).setLore(new ArrayList<>());
                String iName = name == null ? " " : name;
                if (old && iName.length() > 32)
                    iName = iName.substring(0, 32);
                inv = Bukkit.createInventory(this, line * 9, iName);
                for (int i = 0; i < line * 9; i++) {
                    if (content[i] == null) {
                        inv.setItem(i, fonPanel.build());
                        continue;
                    }
                    ItemStack is = content[i].build();
                    ItoB.put(is, content[i]);

                    inv.setItem(i, is);
                }


                p.openInventory(inv);
                GUIController.addOpen(p);
            } else {
                Main.sendMSGColor(ChatColor.RED + "Not open Inventory");
            }
        });
    }

    @Override
    public Inventory getInventory() {
        return inv;
    }

    public void destroy() {
        inv.clear();
        inv = null;
        name = null;
        for (Item_builder i : content) {
            if (i != null) i.destroy();
        }

    }

    public void update() {
        Item_builder fonPanel = new Item_builder(constructor.fon).name(ChatColor.RESET.toString()).setLore(new ArrayList<>());
        ItoB.clear();
        inv.clear();
        for (int i = 0; i < line * 9; i++) {
            if (content[i] == null) {
                inv.setItem(i, fonPanel.build());
                continue;
            }
            ItemStack is = content[i].build();
            ItoB.put(is, content[i]);
            inv.setItem(i, is);
        }
    }
}