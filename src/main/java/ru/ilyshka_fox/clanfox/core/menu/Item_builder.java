package ru.ilyshka_fox.clanfox.core.menu;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;

import javax.annotation.Nullable;
import java.lang.reflect.Field;
import java.util.*;

import static com.google.common.base.Preconditions.*;

/**
 * Builder pattern for an {@link ItemStack}.
 *
 * @author Lukas Nehrke
 * @version 1.0
 */
public final class Item_builder {

    private static final Random r = new Random();

    @Nullable
    private Material material = Material.STONE;
    @Nullable
    private String name = " ";
    @Nullable
    private List<String> lore = new ArrayList<>();
    @Nullable
    private String profil;
    @Nullable
    private ItemListener run;

    @Nullable
    private Integer amount = 1;
    @Nullable
    private MaterialData data;
    @Nullable
    private Short durability = 0;

    @Nullable
    private String localizedName;
    @Nullable
    private Boolean unbreakable;
    @Nullable
    private ItemFlag[] flags;


    /**
     * Creates a new {@link Item_builder} instance.
     */
    public Item_builder() {
    }

    public Item_builder(itemHead config) {
        if (config.name != null)
            setName(config.getName());
        if (config.lore != null) {
            setLore(config.getLore());
        }
        if (config.material != null)
            material(config.getMaterial());
        if (config.data != null)
            configData(config.getData());

    }

    public Item_builder(itemHead config, ListenerReplace rep) {
        if (config.name != null)
            setName(rep.replase(config.getName()));
        if (config.lore != null) {
            setLore(new ArrayList<>(config.lore));
            lore.replaceAll(rep::replase);
        }
        if (config.material != null)
            material(config.getMaterial());
        if (config.data != null)
            configData(config.getData());
    }

    public Item_builder replaseAll(ListenerReplace replase) {
        setName(replase.replase(getName()));
        lore.replaceAll(s -> replase.replase(s));
        return this;
    }

    public String getName() {
        return name;
    }

    public List<String> getLore() {
        return lore;
    }

    public Material getMaterial() {
        return material;
    }

    public Item_builder ItemListener(ItemListener run) {
        this.run = run;
        return this;
    }

    public ItemListener getRun() {
        return run;
    }

    /**
     * Sets the {@link Material} of this {@link ItemStack}.
     * <p>
     * You can also use {@link Item_builder#material(int)} with the material id.
     */
    public Item_builder material(Material material) {
        this.material = material;
        return this;
    }

    /**
     * Sets the {@link Material} of this {@link ItemStack} using its unique id.
     *
     * @throws IllegalArgumentException if the material id is negative or the id does not belong to
     *                                  any material
     */
    @SuppressWarnings("deprecation")
    public Item_builder material(int id) {
        checkArgument(id >= 0);
        final Material material = Material.getMaterial(id);
        checkArgument(material != null, "Material could not be found");
        this.material = material;
        return this;
    }

    /**
     * Sets the {@link Material} of this {@link ItemStack} using its name.
     *
     * @throws IllegalArgumentException if the id does not belong to any material
     */
    public Item_builder material(String name) {
        final Material material = Material.getMaterial(checkNotNull(name, "Name cannot be null"));
        checkArgument(material != null, "Material could not be found (" + name + ")");
        this.material = material;
        return this;
    }

    /**
     * Sets the {@link ItemStack}'s quantity.
     * <p>
     * The value must be greater than zero or an exception will be thrown.
     *
     * @throws IllegalArgumentException if the amount is not greater than zero
     */
    public Item_builder amount(int amount) {
        checkArgument(amount > 0);
        this.amount = amount;
        return this;
    }

    /**
     * Sets the {@link MaterialData} for this {@link ItemStack}.
     * <p>
     * You can also use {@link Item_builder#data(byte)} to set the data by its
     * id.
     *
     * @see MaterialData
     */
    public Item_builder data(MaterialData data) {
        this.data = data;
        return this;
    }

    /**
     * Sets the {@link MaterialData} for this {@link ItemStack} by its id.
     *
     * @see MaterialData
     */
    @SuppressWarnings("deprecation")
    public Item_builder data(byte data) {
        this.data = new MaterialData(data);
        return this;
    }

    /**
     * Sets the durability of this {@link ItemStack}.
     * <p>
     * The durability can not be lower than zero.
     *
     * @throws IllegalArgumentException if {@code durability} is negative
     */
    public Item_builder durability(short durability) {
        checkArgument(durability >= 0);
        this.durability = durability;
        return this;
    }

    public Item_builder profil(String profil) {
        this.profil = profil;
        return this;
    }

    /**
     * Sets the {@code Displayname} for this {@link ItemStack}.
     * <p>
     * Color-Codes are supported.
     */
    public Item_builder name(String name) {
        this.name = ChatColor.translateAlternateColorCodes('&', name);
        return this;
    }

    public void setName(String name) {
        if (name == null) name = "";
        this.name = name;
    }

    /**
     * Sets the localized name for this {@link ItemStack}.
     *
     * @see ItemMeta#setLocalizedName(String)
     */
    public Item_builder localizedName(String localizedName) {
        this.localizedName = localizedName;
        return this;
    }

    /**
     * Makes the {@link ItemStack} unbreakable (it does not loose durability).
     * <p>
     * Use{@link ItemFlag#HIDE_UNBREAKABLE} to hide this attribute.
     *
     * @param unbreakable true to make the item unbreakable
     * @see ItemMeta#setUnbreakable(boolean)
     */
    public Item_builder unbreakable(boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    /**
     * Adds a list of {@link ItemFlag}s to this {@link ItemStack}.
     *
     * @see ItemFlag
     */
    public Item_builder flags(ItemFlag... flags) {
        this.flags = flags;
        return this;
    }

    /**
     * Sets the lore for this {@link ItemStack}.
     * <p>
     * This is the text that will be displayed below the Displayname on
     * hovering.
     */
    public Item_builder setLore(List<String> lore) {
        this.lore.clear();
        this.lore.addAll(lore);
        return this;
    }

    /**
     * Sets the lore for this {@link ItemStack}.
     * <p>
     * This is the text that will be displayed below the Displayname on
     * hovering.
     */
    public Item_builder setLore(Collection<String> lore) {
        this.lore = (List<String>) lore;
        return this;
    }

    public Item_builder configData(String configData) {
        if (configData == null) configData = "0";
        try {
            int k = Integer.valueOf(configData);
            durability = (short) k;
        } catch (Exception e) {
            profil = configData;
            durability = (short) 3;
        }
        return this;
    }

    /**
     * Builds an {@link ItemStack} with the requested features.
     *
     * @return the requested itemstack
     * @throws IllegalStateException if no material has been set
     */
    public ItemStack build() {

        final ItemStack im = new ItemStack(material);
        if (amount != null) {
            im.setAmount(amount);
        }
        if (data != null) {
            im.setData(data);
        }
        if (durability != null) {
            im.setDurability(durability);
        }
        if (localizedName != null) {
            localizedName = "ID: " + String.valueOf(r.nextLong());
        }
        if (profil != null && material == Material.SKULL_ITEM) {
            SkullMeta meta = (SkullMeta) im.getItemMeta();
            im.setDurability((short) 3);
            if (name != null) {
                meta.setDisplayName(name);
            }
            meta.setLocalizedName(localizedName);
            if (unbreakable != null) {
                meta.setUnbreakable(unbreakable);
            }
            if (flags != null) {
                meta.addItemFlags(flags);
            }
            if (lore != null) {
                meta.setLore(lore);
            }
            GameProfile localGameProfile = new GameProfile(java.util.UUID.randomUUID(), null);
            localGameProfile.getProperties().put("textures", new Property("textures", this.profil));
            Field localField = null;
            try {
                localField = meta.getClass().getDeclaredField("profile");
                localField.setAccessible(true);
                localField.set(meta, localGameProfile);
            } catch (Exception Exception) {
            }
            im.setItemMeta(meta);
        } else {
            final ItemMeta meta = im.getItemMeta();
            if (name != null) meta.setDisplayName(name);
            meta.setLocalizedName(localizedName);
            if (unbreakable != null) meta.setUnbreakable(unbreakable);
            if (flags != null) meta.addItemFlags(flags);
            if (lore != null) meta.setLore(lore);
            im.setItemMeta(meta);
        }

        return im;
    }

    /**
     * Creates a {@link String} with all set attributes of the current
     * {@link Item_builder} instance.
     */

    @Override
    public Item_builder clone() {
        return new Item_builder()
                .material(material).amount(amount).data(data).durability(durability).name(name)
                .localizedName(localizedName).unbreakable(unbreakable).flags(flags).setLore(lore)
                .ItemListener(run);
    }

    @Override
    public String toString() {
        return build().toString();
    }


    public void destroy() {
        material = null;
        name = null;
        if (lore != null)
            lore.clear();
        lore = null;
        profil = null;
        run = null;
        amount = null;
        data = null;
        durability = null;
        localizedName = null;
        unbreakable = null;
        flags = null;
    }
}
