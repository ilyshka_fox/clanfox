package ru.ilyshka_fox.clanfox.core.menu.anvil;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import ru.ilyshka_fox.clanfox.menus.defaylt.ErrorMenu;

import java.util.HashMap;

public class AnvilHolder implements Listener {

    static HashMap<Inventory, AnvilLisenger> opens = new HashMap<>();
    final static String ver = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
    private static Class<?> entityPlayerClass;
    private static Class<?> сraftPlayerClass;
    private static Class<?> containerAnvilClass;
    private static Class<?> entityHumanClass;
    private static Class<?> blockPositionClass;
    private static Class<?> playerConnectionClass;
    private static Class<?> packetPlayOutOpenWindowClass;
    private static Class<?> chatMessageClass;
    private static Class<?> iChatBaseComponentClass;
    private static Class<?> iCraftingClass;
    private static Class<?> packetClass;

    static {
        try {
            entityPlayerClass = Class.forName("net.minecraft.server." + ver + ".EntityPlayer");
            сraftPlayerClass = Class.forName("org.bukkit.craftbukkit." + ver + ".entity.CraftPlayer");
            containerAnvilClass = Class.forName("net.minecraft.server." + ver + ".ContainerAnvil");
            entityHumanClass = Class.forName("net.minecraft.server." + ver + ".EntityHuman");
            blockPositionClass = Class.forName("net.minecraft.server." + ver + ".BlockPosition");
            playerConnectionClass = Class.forName("net.minecraft.server." + ver + ".PlayerConnection");
            packetPlayOutOpenWindowClass = Class.forName("net.minecraft.server." + ver + ".PacketPlayOutOpenWindow");
            chatMessageClass = Class.forName("net.minecraft.server." + ver + ".ChatMessage");
            iChatBaseComponentClass = Class.forName("net.minecraft.server." + ver + ".IChatBaseComponent");
            iCraftingClass = Class.forName("net.minecraft.server." + ver + ".ICrafting");
            packetClass = Class.forName("net.minecraft.server." + ver + ".Packet");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param p
     * @param lis
     * @return
     */
    public static AnvilInventory open(Player p, AnvilLisenger lis) {

        try {

            Object entityPlayer = сraftPlayerClass.getMethod("getHandle").invoke(p);
            Object сontainerAnvil = containerAnvilClass.getConstructors()[0].newInstance(
                    entityHumanClass.getField("inventory").get(entityPlayer),
                    entityHumanClass.getField("world").get(entityPlayer),
                    blockPositionClass.getConstructor(int.class, int.class, int.class).newInstance(0, 0, 0),
                    entityPlayer);
            containerAnvilClass.getField("checkReachable").set(сontainerAnvil, false);
            int containerId = (int) entityPlayerClass.getMethod("nextContainerCounter").invoke(entityPlayer);
            Object chatMessage = chatMessageClass.getConstructor(String.class, Object[].class).newInstance("Repairing",
                    new Object[]{});
            Object packetPlayOutOpenWindow = packetPlayOutOpenWindowClass
                    .getConstructor(int.class, String.class, iChatBaseComponentClass, int.class)
                    .newInstance(containerId, "minecraft:anvil", chatMessage, 0);
            playerConnectionClass.getMethod("sendPacket", packetClass)
                    .invoke(entityPlayerClass.getField("playerConnection").get(entityPlayer), packetPlayOutOpenWindow);
            containerAnvilClass.getField("windowId").set(сontainerAnvil, containerId);
            containerAnvilClass.getMethod("addSlotListener", iCraftingClass).invoke(сontainerAnvil, entityPlayer);
            entityPlayerClass.getField("activeContainer").set(entityPlayer, сontainerAnvil);
            containerAnvilClass.getMethod("getBukkitView").invoke(сontainerAnvil);
            InventoryView iv = (InventoryView) containerAnvilClass.getMethod("getBukkitView").invoke(сontainerAnvil);
            AnvilInventory i = (AnvilInventory) iv.getTopInventory();
            lis.open(i);
            opens.put(i, lis);
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            ErrorMenu.open(p);
        }

        return null;
    }

    public static void closeAll() {
        opens.forEach((x, y) -> {
            x.clear();
            x.getViewers().forEach(q -> q.closeInventory());
        });
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        if (e.getInventory().getType() == InventoryType.ANVIL && e.getInventory() instanceof AnvilInventory) {
            if (opens.containsKey(e.getInventory())) {
                e.getInventory().clear();
                opens.get(e.getInventory()).close();
                opens.remove(e.getInventory());
            }
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {

        Inventory clickedInventory;
        int slot = e.getRawSlot();
        if (slot < 0) {
            clickedInventory = null;
        } else if (e.getView().getTopInventory() != null && slot < e.getView().getTopInventory().getSize()) {
            clickedInventory = e.getView().getTopInventory();
        } else {
            clickedInventory = e.getView().getBottomInventory();
        }


        if (clickedInventory != null && clickedInventory.getType() == InventoryType.ANVIL) {
            if (opens.containsKey(clickedInventory)) {
                e.setCancelled(true);
                opens.get(clickedInventory).onInventoryClick(e);
            }
        }
    }

    @EventHandler
    public void onPrepareAnvil(PrepareAnvilEvent e) {
        if (e.getInventory() instanceof AnvilInventory) {
            if (opens.containsKey(e.getInventory())) {
                opens.get(e.getInventory()).onPrepareAnvil(e);
                Player p = (Player) e.getView().getPlayer();
                p.updateInventory();
            }
        }
    }
}
