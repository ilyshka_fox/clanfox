package ru.ilyshka_fox.clanfox.core.sql;

import javafx.print.Collation;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import ru.ilyshka_fox.clanfox.Data;
import ru.ilyshka_fox.clanfox.Main;
import ru.ilyshka_fox.clanfox.core.yamController.global.messages;
import ru.ilyshka_fox.clanfox.data.Clan;
import ru.ilyshka_fox.clanfox.data.ClanNews;
import ru.ilyshka_fox.clanfox.data.ClanPlayers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class dbManager {
    /**
     * Проверяет наличие иг
     * рока в бд
     *
     * @param name имя игрока.
     * @return {@link Boolean}
     */
    public static boolean hasAccount(String name) throws Exception {
        return (boolean) Main.sql.executeCunsQuery(resSet -> resSet.next() && (resSet.getInt(1) > 0), Main.QSQL.checkAccaynt, name);
    }

    /**
     * Проверяет наличие мени клана в бд
     *
     * @param clan имя клана.
     * @return {@link Boolean}
     */
    public static boolean hasClan(String clan) throws Exception {
        return (boolean) Main.sql.executeCunsQuery(resSet -> resSet.next() && (resSet.getInt(1) > 0), Main.QSQL.hasClan, ChatColor.stripColor(clan));
    }

    /**
     * Проверяет наличие тега клана в бд
     *
     * @return {@link Boolean}
     */
    public static boolean hasTag(String tag) throws Exception {
        return (boolean) Main.sql.executeCunsQuery(resSet -> resSet.next() && (resSet.getInt(1) > 0), Main.QSQL.hasTag, tag);
    }

    /**
     * Получем вечь топ
     *
     * @return List
     * @throws Exception Ошибка запроса sql
     */
    public static List<Clan> getTop() throws Exception {
        return getTop(-1);
    }

    /**
     * Получаем чать топа
     *
     * @param koll Сколько записей вывести
     * @return List
     * @throws Exception Ошибка запроса sql
     */
    @SuppressWarnings("unchecked")
    public static List<Clan> getTop(int koll) throws Exception {
        return (List<Clan>) Main.sql.executeCunsQuery((ResultSet resSet) -> {
            List<Clan> top = new ArrayList<>();
            while (resSet.next()) {
                Clan c = new Clan(resSet.getInt("ID"));
                c.setCollMembers(resSet.getInt("Members"));
                c.setName(resSet.getString("name"));
                c.setColorname(resSet.getString("colorName"));
                c.setDescription(resSet.getString("descriptions"));
                c.setHead(resSet.getString("icons"));
                c.setTop(resSet.getRow());
                c.setTag(resSet.getString("tag"));
                c.setType(resSet.getInt("type"));
                c.setYp(resSet.getInt("Point"));
                c.setClanHome(resSet.getString("location"));
                top.add(c);
            }
            return top;
        }, koll == -1 ? Main.QSQL.getTop : Main.QSQL.getTopLimit, koll == -1 ? null : koll);
    }


    public static void setLocation(int ClanID, String loc) throws Exception {
        Main.sql.executeCuns(Main.QSQL.setLocation, loc, ClanID);
    }


    /**
     * Колличество кланов в бд
     *
     * @return int
     * @throws Exception Ошибка запроса sql
     */
    public static int getCountClan() throws Exception {
        return (int) Main.sql.executeCunsQuery(resSet -> {
            if (resSet.next())
                return resSet.getInt(1);
            return 0;
        }, Main.QSQL.getCountClan);
    }

    /**
     * Получем инфоранию об игроке и если нету то ошибка
     *
     * @param name имя игрока
     * @return ClanPlayers
     * @throws Exception Ошибка запроса sql
     */
    public static ClanPlayers getClanPlayers(String name) throws Exception {
        ClanPlayers cp = new ClanPlayers();
        Main.sql.executeCunsQuery((resSet) -> {
            if (resSet.next()) {
                cp.setClanPlayers(new ClanPlayers(
                        resSet.getString("name"),
                        resSet.getInt("ID"),
                        resSet.getInt("kills"),
                        resSet.getInt("death"),
                        resSet.getDouble("K/D"),
                        resSet.getString("skin"),
                        resSet.getLong("exit"),
                        resSet.getInt("Clan"),
                        resSet.getInt("lvl"),
                        resSet.getLong("msg")
                ));

                return null;
            }
            throw new Exception("No profile of the player in the database");
        }, Main.QSQL.getClanPlayers, name);
        return cp;
    }

    /**
     * Получаем клан по идентификатор
     *
     * @param id идентификатор клана
     * @return Clan
     * @throws Exception Ошибка запроса sql
     */
    public static Clan getClan(int id) throws Exception {
        return (Clan) Main.sql.executeCunsQuery(resSet -> {
            if (resSet.next()) {
                return new Clan(id)
                        .setCollMembers(resSet.getInt("Members"))
                        .setName(resSet.getString("name"))
                        .setColorname(resSet.getString("colorName"))
                        .setTag(resSet.getString("tag"))
                        .setDescription(resSet.getObject("descriptions") == null ? "" : resSet.getString("descriptions"))
                        .setHead(resSet.getString("icons"))
                        .setType(resSet.getInt("type"))
                        .setYp(resSet.getDouble("Point"))
                        .setTop(resSet.getInt("top") + 1)
                        .setClanHome(resSet.getString("location"));
            }
            return null;
        }, Main.QSQL.getClan, id);
    }

    /**
     * Получить клан и если его нету то вызвать ошибку.
     *
     * @param id инфификатор клана
     * @return Clan
     * @throws Exception Ошибка запроса sql
     */
    public static Clan getClanThrows(int id) throws Exception {
        Clan c = getClan(id);
        if (c == null) {
            throw new Exception("By not learn to get the clan " + id + " from the db");
        }
        return c;
    }

    @SuppressWarnings("unchecked")
    public static ArrayList<ClanNews> getClanNews(int id, int limit) throws Exception {
        return (ArrayList<ClanNews>) Main.sql.executeCunsQuery(resSet -> {
            ArrayList<ClanNews> news = new ArrayList<>();
            while (resSet.next()) {
                ClanNews b = new ClanNews(resSet.getString("Player"), resSet.getString("msg"), resSet.getString("skin"), resSet.getLong("time"));
                news.add(b);
            }
            return news;
        }, Main.QSQL.getNews, id, limit);
    }

    /**
     * Получить список участников клана
     *
     * @param id Клана
     * @return List
     * @throws Exception Ошибка запроса sql
     */
    public static ArrayList<ClanPlayers> getClanMembers(int id) throws Exception {
        ArrayList<ClanPlayers> Mem = new ArrayList<>();
        Main.sql.executeCunsQuery(resSet -> {
            while (resSet.next()) {
                ClanPlayers b = new ClanPlayers().setClanPlayers(new ClanPlayers(
                        resSet.getString("name"),
                        resSet.getInt("ID"),
                        resSet.getInt("kills"),
                        resSet.getInt("death"),
                        resSet.getDouble("K/D"),
                        resSet.getString("skin"),
                        resSet.getLong("exit"),
                        id,
                        resSet.getInt("lvl"),
                        resSet.getLong("msg")));
                Mem.add(b);
            }
            return null;
        }, Main.QSQL.getClanMembers, id);
        return Mem;
    }


    // проверка наличия e buhjrf rkyf
    @SuppressWarnings("unchecked")
    public static boolean isPlayerClan(int PlayerID) throws Exception {
        return ((boolean) Main.sql.executeCunsQuery(resSer -> resSer.next() && resSer.getInt(1) > 0, Main.QSQL.isPlayerIDClan, PlayerID));
    }

    /**
     * Обновить статус игрока в клане.
     *
     * @param playerId идентификатор игрока
     * @param rank     номер ранга игрока
     * @throws Exception Ошибка запроса sql
     */
    public static void UpdateRank(int playerId, int rank) throws Exception {
        Main.sql.executeCuns(Main.QSQL.UpdateRank, rank - 1, playerId);
    }

    /**
     * Удалить игрока из клана
     *
     * @param playerId идентификатор игрока
     * @throws Exception Ошибка запроса sql
     */
    public static void kickMembers(int clanid, int playerId) throws Exception {
        Main.sql.executeCuns(Main.QSQL.kickMember, playerId);
        Main.sql.executeCuns(Main.QSQL.addBanNameInClan, clanid, playerId);

    }

    /**
     * Написать сообщение в клан
     *
     * @param p      игрок который отправил его
     * @param clanID клан в котором отправили
     * @param title  заголовок сообщениея
     * @param msg    сообщение
     * @param head   строка головы. иконка сообшения
     * @param time   обнулить счедчик для игрока
     * @throws Exception Ошибка запроса sql
     */
    public static void sendClanMsg(Player p, int clanID, String title, String msg, String head, boolean time) throws Exception {
        List<String> newMessage = new ArrayList<>(messages.newMessageChat);
        newMessage.replaceAll(s -> s.replaceFirst("<title>", title).replaceFirst("<msg>", msg));
        Main.sql.executeCunsQuery(resSer -> {
            while (resSer.next()) {
                Player player = Bukkit.getPlayer(resSer.getString(1));
                if (player != null) {
                    Bukkit.getScheduler().runTask(Main.plugin, () -> {
                        newMessage.forEach(player::sendMessage);
                    });
                }
            }
            return null;
        }, Main.QSQL.getClanMemberName, clanID);

        Main.sql.execute(Main.QSQL.newMsg, clanID, title, msg, head, System.currentTimeMillis());
        if (time)
            Main.sql.execute(Main.QSQL.setTime, System.currentTimeMillis(), p.getName());
    }

    /**
     * Количество заявок в клан
     *
     * @param ClanID инфификатор клана
     * @return int
     * @throws Exception Ошибка запроса sql
     */
    public static int getCountRequest(int ClanID) throws Exception {
        return (int) Main.sql.executeCunsQuery(resSer -> {
            if (resSer.next()) {
                return resSer.getInt(1);
            }
            return -1;
        }, Main.QSQL.getCountRequest, ClanID);
    }

    /**
     * Количество приглашений в клан у игрока
     *
     * @param PlayerID идентификатор игрока
     * @return int
     * @throws Exception Ошибка запроса sql
     */
    public static int getCountInvitation(int PlayerID) throws Exception {
        return (int) Main.sql.executeCunsQuery(resSer -> {
            if (resSer.next()) {
                return resSer.getInt(1);
            }
            return -1;
        }, Main.QSQL.getCountInvations, PlayerID);
    }

    /**
     * Получить приглашения вклан у игрока.
     *
     * @param playerID инфификатор игрока
     * @return List
     * @throws Exception Ошибка запроса sql
     */
    public static ArrayList<Clan> getClansInvitation(int playerID) throws Exception {
        ArrayList<Clan> clans = new ArrayList<>();

        Main.sql.executeCunsQuery(resSet -> {
            while (resSet.next()) {
                Clan c = new Clan(resSet.getInt("ID"));
                c.setCollMembers(resSet.getInt("Members"));
                c.setName(resSet.getString("name"));
                c.setColorname(resSet.getString("colorName"));
                c.setDescription(resSet.getString("descriptions"));
                c.setHead(resSet.getString("icons"));
                c.setTag(resSet.getString("tag"));
                c.setType(resSet.getInt("type"));
                c.setYp(resSet.getInt("Point"));
                c.setClanHome(resSet.getString("location"));
                clans.add(c);
            }
            return null;
        }, Main.QSQL.getClansInvation, playerID);

        return clans;
    }

    /**
     * Получить заявки в клан
     *
     * @param ClanID
     * @return List
     * @throws Exception Ошибка запроса sql
     */
    public static ArrayList<ClanPlayers> getInvitation(int ClanID) throws Exception {
        ArrayList<ClanPlayers> cp = new ArrayList<>();
        Main.sql.executeCunsQuery(resSet -> {
            while (resSet.next()) {
                if (resSet.getObject("Clan") != null) {
                    clearPlayerRequest(resSet.getInt("ID"));
                    continue;
                }
                cp.add(new ClanPlayers(
                        resSet.getString("name"),
                        resSet.getInt("ID"),
                        resSet.getInt("kills"),
                        resSet.getInt("death"),
                        resSet.getInt("K/D"),
                        resSet.getString("skin"),
                        resSet.getLong("exit")
                ));
            }
            return null;
        }, Main.QSQL.getRequest, ClanID);
        return cp;
    }

    /**
     * Удалить все заявки в клан у игрока
     **/
    public static void clearPlayerRequest(int playerID) throws Exception {
        Main.sql.executeCuns(Main.QSQL.clearRequest, playerID);
    }

    /**
     * Удалить все приглашения кланов у игрока
     **/
    public static void clearPlayerInvitation(int playerID) throws Exception {
        Main.sql.executeCuns(Main.QSQL.clearInvation, playerID);
    }

    /**
     * Удалить заявку в клан
     **/
    public static void removePlayerRequest(int playerID, int ClanID) throws Exception {
        Main.sql.executeCuns(Main.QSQL.removeRequest, playerID, ClanID);
        Main.sql.executeCuns(Main.QSQL.addBanNameInClan, ClanID, playerID);
    }

    /**
     * Удалить приглашение в клан
     *
     * @param playerID игрок
     * @param ClanID   клан
     * @throws Exception Ошибка запроса sql
     */
    public static void removedPlayerInvitation(int playerID, int ClanID) throws Exception {
        Main.sql.executeCuns(Main.QSQL.removeInvation, playerID, ClanID);
    }

    /**
     * Удалить бан в клане
     *
     * @param playerID игрок
     * @param ClanID   клан
     * @throws Exception Ошибка запроса sql
     */
    public static void removedPlayerBan(int playerID, int ClanID) throws Exception {
        Main.sql.executeCuns(Main.QSQL.remBanNameInClan, playerID, ClanID);
    }

    /**
     * Установить игроку клан
     *
     * @param ClanId   клан
     * @param playerID игрок
     * @throws Exception Ошибка запроса sql
     */
    public static void setPlayerClan(String name, int ClanId, int playerID) throws Exception {
        Main.sql.executeCuns(Main.QSQL.setClan, playerID, ClanId, 0);
        clearPlayerRequest(playerID);
        clearPlayerInvitation(playerID);
        removedPlayerBan(playerID, ClanId);
        removeCash(name);
    }

    // переименовать
    public static void reNameClan(int ClanID, String name) throws Exception {
        Main.sql.executeCuns(Main.QSQL.reNameClan, name, ChatColor.stripColor(name), ClanID);
    }

    // Изменить тег клана
    public static void reNameTag(int ClanID, String tag) throws Exception {
        Main.sql.executeCuns(Main.QSQL.reNameTag, tag, ClanID);
    }

    // Изменить Иконку клана
    public static void reIconClan(int ClanID, String icons) throws Exception {
        Main.sql.executeCuns(Main.QSQL.reIconClan, icons, ClanID);
    }

    // Изменить описание
    public static void reDiscriptions(int ClanID, String description) throws Exception {
        Main.sql.executeCuns(Main.QSQL.reDescription, description, ClanID);
    }

    // Удалить клан
    public static void DesbandClan(int ClanID) throws Exception {
        Main.sql.executeCuns(Main.QSQL.DisbandClan, ClanID);
        Main.sql.executeCuns("DELETE FROM `Data` WHERE `ClanID` = ?", ClanID);
        Main.sql.executeCuns("DELETE FROM `PlayerRequest` WHERE `ClanID` = ?", ClanID);
        Main.sql.executeCuns("DELETE FROM `Ban` WHERE `ClanID` = ?", ClanID);
        Main.sql.executeCuns("DELETE FROM `ClanInvite` WHERE `ClanID` = ?", ClanID);
        Main.sql.executeCuns("DELETE FROM `News` WHERE `ClanID` = ?", ClanID);

        System.out.print(kesClan.keySet().size() + " | " + kestime.keySet().size());
        Iterator<Map.Entry<String, Integer>> i = kesClan.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry<String, Integer> en = i.next();
            if (en.getValue() == ClanID) {
                kestime.remove(en.getKey());
                i.remove();
            }
        }
        System.out.print(kesClan.keySet().size() + " | " + kestime.keySet().size());
    }

    // покинуть клан
    public static void exitClan(String name) throws Exception {
        Main.sql.executeCuns(Main.QSQL.ExitNameClan, name);
    }

    public static void exitClan(int PlayerID) throws Exception {
        Main.sql.executeCuns(Main.QSQL.ExitIdClan, PlayerID);
    }

    // проверка наличия приглашения в клан
    @SuppressWarnings("unchecked")
    public static boolean isClanInvate(int ClanID, int PlayerID) throws Exception {
        return ((boolean) Main.sql.executeCunsQuery(resSer -> resSer.next() && resSer.getInt(1) > 0, Main.QSQL.chackClanInvate, ClanID, PlayerID));
    }

    // проверка наличия заявки в клан
    @SuppressWarnings("unchecked")
    public static boolean isClanRequest(int ClanID, int PlayerID) throws Exception {
        return ((int) Main.sql.executeCunsQuery(resSer -> {
            if (resSer.next()) {
                return resSer.getInt(1);
            }
            return 0;
        }, Main.QSQL.chackClanReqvesr, ClanID, PlayerID)) != 0;
    }

    // отправить приглашение
    public static void setClanInvate(int ClanID, int PlayerID) throws Exception {
        Main.sql.executeCuns(Main.QSQL.clanSendInvate, ClanID, PlayerID);
    }

    // проверка наличия бана в клане
    public static boolean isBanNameInClan(int ClanID, int PlayerID) throws Exception {
        return ((boolean) Main.sql.executeCunsQuery(resSer -> resSer.next() && resSer.getInt(1) > 0, Main.QSQL.isBanNameInClan, PlayerID, ClanID));
    }

    // новая заявка в клан
    public static boolean newRequest(int ClanID, int PlayerID) throws Exception {
        return Main.sql.executeCuns(Main.QSQL.newRequest, ClanID, PlayerID);
    }


    static HashMap<String, Integer> kesClan = new HashMap<>();
    static HashMap<String, Long> kestime = new HashMap<>();


    public static boolean isOneClansPlayers(String p1, String p2) {
        if (kesClan.containsKey(p1) && kesClan.containsKey(p2))
            if (kestime.containsKey(p1) && kestime.containsKey(p2))
                if (kestime.get(p1) + 60000 > System.currentTimeMillis() && kestime.get(p2) + 60000 > System.currentTimeMillis()) {
                    int i1 = kesClan.get(p1);
                    int i2 = kesClan.get(p2);
                    return (i1 == i2) && (i1 != 0);
                }
        try {
            return (boolean) Main.sql.executeCunsQuery(resSet -> {
                int i1 = 0;
                int i2 = 0;
                while (resSet.next()) {
                    if (resSet.getString("name").equalsIgnoreCase(p1)) {
                        i1 = resSet.getInt("ClanID");
                    } else if (resSet.getString("name").equalsIgnoreCase(p2)) {
                        i2 = resSet.getInt("ClanID");
                    }
                }
                kesClan.put(p1, i1);
                kesClan.put(p2, i2);
                kestime.put(p1, System.currentTimeMillis());
                kestime.put(p2, System.currentTimeMillis());
                resSet.getStatement().close();
                return (i1 == i2) && (i1 != 0);
            }, "SELECT `name`,`ClanID` FROM `Data` INNER JOIN `Players` ON `Players`.`ID` = `Data`.`PlayerID` WHERE  (`Players`.`name` = ?) OR  (`Players`.`name` = ?)", p1, p2);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void removeCash(String name) {
        kestime.remove(name);
        kesClan.remove(name);
    }


    public static void addDeath(String name) {
        Main.sql.execute("UPDATE `Players` SET `death` = `death` + 1 WHERE `name` = ?", name);
    }

    public static void addKill(String name) {
        Main.sql.execute("UPDATE `Players` SET `kills` = `kills` + 1 WHERE `name` = ?", name);
    }
}
