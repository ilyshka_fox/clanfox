package ru.ilyshka_fox.clanfox.core.yamController;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Contain {
    String path() default "";

    String[] comment() default {};

    String config() default "Config";

    boolean fullPath() default false;
}
