package ru.ilyshka_fox.clanfox.core.yamController;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Embedded {
    String path() default "";

    String[] comment() default {};
}
