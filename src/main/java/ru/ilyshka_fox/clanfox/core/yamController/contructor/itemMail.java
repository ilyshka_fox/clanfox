package ru.ilyshka_fox.clanfox.core.yamController.contructor;

import ru.ilyshka_fox.clanfox.core.yamController.Comments;
import ru.ilyshka_fox.clanfox.core.yamController.Value;

import java.util.HashMap;
import java.util.List;

public class itemMail {
    @Value
    public String title;
    @Value
    public String msg;
    @Value
    public String head;

    @Comments
    HashMap<String, List<String>> comments;

    public itemMail(String title, String msg, String head, HashMap<String, List<String>> comments) {
        this.title = title;
        this.msg = msg;
        this.head = head;
        this.comments = comments;
    }
}
