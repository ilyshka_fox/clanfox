package ru.ilyshka_fox.clanfox.core.yamController.global;

import org.bukkit.ChatColor;
import ru.ilyshka_fox.clanfox.core.yamController.*;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


@Contain(config = "Setting")
public class constructor {


    @Value
    public static String noClan = ChatColor.BOLD + "У вас нет клана. (vk - ilyshkafox)";

    @Value
    public static String online = ChatColor.GREEN + "В сети";

    @Value(comment = {"<day> - сколько дней не в сети"})
    public static String offline = ChatColor.RED + "Не в сети <day> день";
    @Value
    public static String owner = "Глава";
    @Value
    public static String coleader = "Руководитель";
    @Value
    public static String elder = "Старейшина";
    @Value
    public static String member = "Участник";
    @Value
    public static String free = "Бесплатно";

    @Comments(type = Comments.TYPE.HEAD)
    public static HashMap<String, List<String>> head = new HashMap<>();


    @Embedded(comment = {"Линии отделяющие части меню"})
    public static itemHead line = new itemHead(null, null, "STAINED_GLASS_PANE", "14", null, null);

    @Embedded(comment = {"Фон для всех меню"})
    public static itemHead fon = new itemHead(null, null, "STAINED_GLASS_PANE", "8", null, null);

    @Embedded(comment = {"Фон для подменю управления"})
    public static itemHead fonSetting = new itemHead(null, null, "STAINED_GLASS_PANE", "3", null, null);


    static {
        List<String> l = Arrays.asList(
                "=======================================================",
                "",
                "               2Xr                     .rX2",
                "               X999i.                 s993X",
                "               593399s              s99999S",
                "               ;9X99399r          r993933hr",
                "                G,:S99393;      ;X9399S:,G.",
                "                XS,,,i932S.    .S239i,,,iX",
                "                ;G,.,...          .,.,,,hr",
                "                 99,                  ,3h",
                "                 2X                    X2",
                "                 :                      :",
                "",
                "  $$$$   $$       $$$$   $$  $$  $$$$$$   $$$$   $$  $$",
                " $$  $$  $$      $$  $$  $$$ $$  $$      $$  $$   $$$$",
                " $$      $$      $$$$$$  $$ $$$  $$$$    $$  $$    $$",
                " $$  $$  $$      $$  $$  $$  $$  $$      $$  $$   $$$$",
                "  $$$$   $$$$$$  $$  $$  $$  $$  $$       $$$$   $$  $$",
                "",
                "                     by ilyshka_fox",
                " =======================================================",
                "                http://vk.com/ilyshkafox",
                "",
                " ======Информация клана======",
                " <clanname>          имя клана",
                " <tag>               Тег клана",
                " <nocolorname>       Имя клана без цвета",
                " <nocolortag>        тег клана без цвета",
                " <clanID>            Идентификатор клана",
                " <point>             Рейтинг клана",
                " <countmembers>      Колличество участников",
                " <descriptions>      Описание клана",
                "",
                " ======Главное меню - поиск кланов======",
                " <countclans>        количество кланов",
                "",
                " ======Информация игрока======",
                " <nickname>           Ник игрока",
                " <kills>             Убийства",
                " <death>             Смерти",
                " <KD>                Убил/Погиб или репутация, если смертей больше чем убийств, то значение уходит в минус",
                " <playerID>          Идентификатор игрока",
                " <rank>              Статус игрока в клане",
                " <online>            Статус игрока на сервере",
                " <clantop>           Место в клане",
                "",
                " ======Конструктор топа======",
                " <top*@>   - Нужно для вывода топа, заменяться номером в топе",
                "      @    - Любое число, колличество строк выводимого топа.",
                " --PS--",
                " Для каждой строчки будет браться данная конструкция и заменяться на нужные данные.",
                " Также в конструктор можно добавить любую информацию о клане кроме описания.",
                " --PPS--",
                " Например: &f<top*3>) &8<clanname> | <clanID>   - выведет 3 строки топа",
                " 1) имяКлана1 | 2",
                " 2) имяКлана2 | 6",
                " 3) имяКлана3 | 16"
        );
        head.put("Info", l);
    }
}
