package ru.ilyshka_fox.clanfox.core.yamController;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import ru.ilyshka_fox.clanfox.Main;
import ru.ilyshka_fox.clanfox.setting.Config;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

// хз как считают другие. но для меня это катыль. труднее но правильнее сделать свою yaml обертку.
public class yamController {
    private static HashMap<String, FileConfiguration> map = new HashMap<>();

    private static HashMap<String, FileConfiguration> configMap = new HashMap<>();

    private static HashMap<String, HashMap<String, List<String>>> commentMap = new HashMap<>();

    //Инициализация переменных. во всех классах.
    public static void init() {
        map.clear();
        configMap.clear();
        commentMap.clear();
        // Получам jav файл.
        try {
            String path = URLDecoder.decode(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8");
            File far = new File(path);
            if (Config.debug)
                Main.sendMSGColor(ChatColor.AQUA + far.getPath());
            JarFile jar = new JarFile(far);
            // Проходи по всем пакетам jar файла и инициализируем все конфиг переменные.
            jar.stream().forEach(yamController::initClass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // проверяем и создаем нужные папки.
        if (!Main.plugin.getDataFolder().isDirectory()) {
            Main.plugin.getDataFolder().mkdirs();
        }
        // Вызываем сохранения конфигов.
        save();
    }

    private static void initClass(JarEntry jarEntry) {
        // Изменяем ненужные элементы из строки класса.
        String _path = jarEntry.getName().replace("/", ".").replace(".class", "");
        try {
            //Получаем класс. если это не путь класса а файла. выйдет ошибка.
            Class _class = Class.forName(_path);
            Contain _contain;
            if (_class != null && (_contain = (Contain) _class.getAnnotation(Contain.class)) != null) {
                if (Config.debug)
                    Main.sendMSGColor(ChatColor.RED + _path);

                //Получаем стандартный путь. который присвоен классу или за путь берем класс.
                String _pathConfig = "global.";
                if (_contain.fullPath()) {
                    _pathConfig = _contain.path();
                } else {
                    _pathConfig += _contain.path().length() == 0 ? _class.getSimpleName() : _contain.path();
                }

                // Получаем имя файла конфига
                String _config = _contain.config();


                //Мапа поментариев
                HashMap<String, List<String>> _com;

                //Проверяем наличие загруженного конфига
                if (commentMap.containsKey(_config)) {
                    _com = commentMap.get(_config); // получаем мапу который уже загружен
                } else {
                    // Создаем новую мапу. пустую
                    _com = new HashMap<>();
                    commentMap.put(_config, _com);
                }


                if (_contain.comment().length > 0) {
                    //Наличие коментария Добавляем ее в мапу коментариев.
                    List<String> comments = Arrays.asList(_contain.comment());
                    _com.put(_pathConfig, comments);
                }


                // Конфиг файла.
                FileConfiguration _file;
                // Проверяем наличие загруженого конфиг файл
                if (configMap.containsKey(_config)) {
                    //Получаем
                    _file = configMap.get(_config);
                } else {
                    // Если его нету, то получаем и загружаем из папки.
                    File f = new File(Main.plugin.getDataFolder(), _config + ".yml");
                    _file = YamlConfiguration.loadConfiguration(f);
                    configMap.put(_config, _file);
                }
                //Инициализируем переменныекласса.
                initContain(_config, _pathConfig, _class, _class, _file);
            }

        } catch (Exception e) {
            // Если произошла ошибка в классе. то нам пофигу. просто пропускаем.
            // в папке клссов может быть файл. из за этого вывходят ошибки.
            // на такие папки нам пофигу
        }

    }

    private static void initContain(final String configName, final String path, final Class clazz, final Object obj, final FileConfiguration fail) {
        //Мы получили
        //config - имя файла,
        //path - стандартный путь в конфиге конфига.
        //clazz - сканирующий класс
        //obj - обекст в котором идет сканирование
        //file - Файл конфигоа для проверки.

        // Конфиг мапа с значениями
        FileConfiguration _default;
        if (map.containsKey(configName)) {
            _default = map.get(configName);// получаю конфиг мапу.
        } else {
            _default = new YamlConfiguration();
            map.put(configName, _default);// устанавливаю пустой конфиг если в мапе нету.
        }

        // Мапа для конментариев.
        HashMap<String, List<String>> _com;
        // Проделоваю тоже самое с коментариями.
        if (commentMap.containsKey(configName)) {
            _com = commentMap.get(configName);
        } else {
            _com = new HashMap<>();
            commentMap.put(configName, _com);
        }

        //Получаем все поля класса.
        for (Field field : clazz.getDeclaredFields()) {
            // Устанавливаем значение того что можно менять поле.
            field.setAccessible(true);
            // Проверяю анотации.
            if (field.isAnnotationPresent(Value.class)) {
                // Обычное поле. Значит получаем значение и делаем махинации.
                Value a = field.getDeclaredAnnotation(Value.class);// Получем само анатацию
                String newPathConfig = path.length() > 0 ? path + "." + (a.path().length() == 0 ? field.getName() : a.path()) : (a.path().length() == 0 ? field.getName() : a.path());
                if (Config.debug)
                    Main.sendMSGColor(String.format(ChatColor.WHITE + "%-15s -> %s", configName, newPathConfig));
                try {
                    // Проверю на значение в загружнном файле конфига и самой переменной
                    if (field.get(obj) != null) {
                        if (fail.get(newPathConfig) != null) {
                            // Если в конфиге есть какоето значение. То обрабатываем его.
                            // Подбираем значение из файлй.
                            Object s = fail.get(newPathConfig);
                            // получив значение поля проверяем ее внутреее значение на null/ если null то нам поле не интересно.
                            if (s != null) {
                                if (field.getType().equals(String.class) && a.chatcolor()) {
                                    // Простая строка и нужно . Записываем значение из фалй предварительно применив цветовые коды.
                                    field.set(obj, ChatColor.translateAlternateColorCodes('&', (String) s));
                                } else if (field.getType().equals(Integer.class)) {
                                    field.set(obj, s);
                                } else if (field.getType().equals(List.class)) {
                                    // Если это лист. то применяем цветовые значение. и всавляем в переменую.
                                    try {
                                        List<String> l = (List<String>) s; // значение в конфиге
                                        List<String> _l = new ArrayList<>(l); // оздаем копию листа.
                                        if (a.chatcolor())//Проверяем значение свойства цвета.
                                            _l.replaceAll(s1 -> ChatColor.translateAlternateColorCodes('&', s1));
                                        field.set(obj, _l);
                                    } catch (Exception e) {
                                        //Малоли))
                                    }

                                } else//Если это не один из вариантов. то просто вставляем в конфиг. Малоли))
                                {
                                    field.set(obj, s);
                                }

                                // то что было в конфиге. ставитм как дефолтное значение на данныую сесию.
                                _default.set(newPathConfig, s);
                            }
                        } else {
                            // Если в конфиге пусто
                            Object _objField = field.get(obj);
                            // получаем значение поля.
                            // записываем его в конфиг.
                            if (field.getType().equals(String.class) && a.chatcolor()) {
                                _objField = ((String) _objField).replace(String.valueOf(ChatColor.COLOR_CHAR), "&");
                                _default.set(newPathConfig, _objField);
                            } else if (field.getType().equals(Integer.class)) {
                                _objField = ((Integer) _objField).intValue();
                                _default.set(newPathConfig, _objField);
                            } else if (field.getType().equals(List.class) && a.chatcolor()) {
                                List<String> l = (List<String>) _objField; // Преобразуем значение в список.
                                List<String> _l = new ArrayList<>(l); //Делаем копию листа.
                                _l.replaceAll(s -> s.replace(String.valueOf(ChatColor.COLOR_CHAR), "&")); // Убераем цветовой код
                                _default.set(newPathConfig, _l); // Устанавливаем знчаеник в стандартную мапу.
                            } else {
                                _default.set(newPathConfig, _objField);
                            }
                        }

                        // Установи стандартные занчение в конфиг мапу и в переменую. проверяемналичие комментариев.
                        if (a.comment().length > 0) { // Размер масива коментариев больше 0
                            List<String> comments = Arrays.asList(a.comment());//Создаем обычный лист коментариев.
                            List<String> l = _com.get(newPathConfig);// Получаем уже созданный лист коментариев
                            if (l == null) {// Если созданого листа нету то создаем новый.
                                l = new ArrayList<>();
                                _com.put(newPathConfig, l);
                            }
                            // Добавляем коментарии в лист.
                            l.addAll(comments);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else if (field.isAnnotationPresent(Embedded.class)) {
                // Коментарий для класса.
                try {
                    Embedded a = field.getDeclaredAnnotation(Embedded.class);
                    String pathConfig = path + "." + (a.path().length() == 0 ? field.getName() : a.path());
                    Object _obj = field.get(obj);
                    // Собрали полный путь для конифга.
                    if (_obj != null) {
                        if (a.comment().length > 0) {
                            List<String> l = _com.get(pathConfig);
                            if (l == null) {
                                l = new ArrayList<>();
                                _com.put(pathConfig, l);
                            }
                            l.addAll(Arrays.asList(a.comment()));
                        }
                        // тк это целый класс. то для полечения внутрених элементов. вызываем рекурсией.
                        initContain(configName, pathConfig, _obj.getClass(), _obj, fail);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (field.isAnnotationPresent(Comments.class)) {
                // Если поле явно имеет указатель коментариев. то эта пама. зписываем в мапу коментариев.
                // кароч. просто переписываез значение предварительно сделав полный путь
                try {
                    Comments a = field.getAnnotation(Comments.class);
                    String pathConfig;
                    switch (a.type()) {
                        case HEAD:
                            pathConfig = "CLAN_FOX_HEAD_COMMENT";
                            break;
                        case BUTTON:
                            pathConfig = "CLAN_FOX_BUTTON_COMMENT";
                            break;
                        default:
                            pathConfig = "";
                            break;
                    }
                    try {
                        HashMap<String, List<String>> com = (HashMap<String, List<String>>) field.get(obj);
                        if (com != null)
                            com.forEach((s, strings) -> {

                                String _path = pathConfig;
                                if (a.type() == Comments.TYPE.CONTENT)
                                    _path = (a.path().length() == 0 ? pathConfig + "." + s : a.path());
                                try {
                                    List<String> l = _com.get(_path);
                                    if (l == null) {
                                        l = new ArrayList<>();
                                        _com.put(_path, l);
                                    }
                                    l.addAll(strings);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                    } catch (Exception e) {
                        // Если не смогди прочитать коментаии. или поле оказалось не мапой. то мне пофигу.
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    // Сохранение файлов.
    public static void save() {
        map.forEach((s, fileConfiguration) -> {
            // Проходим по всем конфигам.


            if (Config.debug)
                Main.sendMSGColor(ChatColor.BLUE + s + ".yml");

            // Создаем новый экземпляр конфига для чистоты. Перезаписываем старый.
            File f = new File(Main.plugin.getDataFolder(), s + ".yml");
            //Получаем цельную строку из конфига.
            String ss = fileConfiguration.saveToString();
            //Делим строку на масив по символу новой строки.
            List<String> _list = new LinkedList<>(Arrays.asList(ss.split("\n")));


            // Создаем лист с помощью функции create
            // Корорая работает рекурсивно. входя все в глубь по пробелам и добавляем в нужные места комментарии
            List<String> _new = new ArrayList<>(create(0, _list.iterator(), commentMap.get(s), ""));


            try {

                FileWriter writer = new FileWriter(f);
                // Записываем дату генерации.
                writer.write("# " + new Date().toString() + "\n");
                writer.write("# ===========================\n");
                if (Config.debug) {
                    Main.sendMSGColor(ChatColor.GREEN + "# " + new Date().toString());
                    Main.sendMSGColor(ChatColor.GREEN + "# ===========================");
                }

                if (commentMap.get(s) != null) {
                    // Записывает заголовок файла.
                    if (commentMap.get(s).get("CLAN_FOX_HEAD_COMMENT") != null) {
                        commentMap.get(s).get("CLAN_FOX_HEAD_COMMENT").forEach(s1 -> {
                            try {
                                writer.write("# " + s1 + "\n");
                                if (Config.debug) {
                                    String log = "# " + s1;
                                    if (log.length() > 100)
                                        log = log.substring(0, 100) + "...";
                                    Main.sendMSGColor(ChatColor.GREEN + log);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    writer.write("\n");
                }

                // Записываей полученный конфиг с добавленными коментариями..
                _new.forEach(sq -> {
                    try {
                        writer.write(sq + "\n");
                        if (Config.debug) {
                            String log = sq;
                            if (log.length() > 100) log = log.substring(0, 100) + "...";
                            if (sq.trim().startsWith("#")) {
                                Main.sendMSGColor(ChatColor.GREEN + log);
                            } else if (sq.trim().startsWith("-")) {
                                Main.sendMSGColor(ChatColor.AQUA + log);
                            } else if (sq.trim().endsWith(":")) {
                                Main.sendMSGColor(ChatColor.YELLOW + log);
                            } else if (sq.trim().endsWith(": []")) {
                                Main.sendMSGColor(ChatColor.YELLOW + log.replace(": []", ": " + ChatColor.AQUA + "[]"));
                            } else {
                                Main.sendMSGColor(ChatColor.RED + log);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                writer.write("\n");
                // Добавляем подвал документа.
                if (commentMap.get(s) != null) {
                    if (commentMap.get(s).get("CLAN_FOX_BUTTON_COMMENT") != null) {
                        commentMap.get(s).get("CLAN_FOX_BUTTON_COMMENT").forEach(s1 -> {
                            try {
                                writer.write("# " + s1 + "\n");
                                if (Config.debug)
                                    Main.sendMSGColor(ChatColor.GREEN + "# " + s1);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    writer.write("\n");
                }
                writer.write("# ===========================\n");
                writer.write("# http://vk.com/ilyshkafox");
                if (Config.debug) {
                    Main.sendMSGColor(ChatColor.GREEN + "# ===========================");
                    Main.sendMSGColor(ChatColor.GREEN + "# http://vk.com/ilyshkafox");
                }
                writer.flush();
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private static String sNext;







    public static List<String> create(int lvl, Iterator<String> list, HashMap<String, List<String>> _com, String path) {
        path += path.length() > 0 ? "." : "";
        List<String> newComment = new ArrayList<>();

        //Количество пробелов
        String st = "";
        for (int i = 0; i < lvl; i++) st += "  ";
        final String start = st;
        //sNext = list.next();
        boolean next = true;
        while (list.hasNext()) {
            if (next) sNext = list.next();
            next = false;
            if (sNext == null) return newComment;
            if (sNext.length() < lvl * 2)//EXIT выход т.к. уровень уменьшился
                return newComment;
            String sNew = sNext.substring(lvl * 2);
            if (sNext.substring(0, lvl * 2).trim().length() != 0) {
                //EXIT выход т.к. уровень уменьшился
                return newComment;
            } else if (sNew.trim().length() == 0) {
                //EMPTY просто пустая строка. Пропускаем ее.
                next = true;
            } else if (sNext.trim().startsWith("-")) {
                //LIST ITEM это элемент лисла. мне тут нечего делать. и просто пропускаю добавив в новый масив.
                newComment.add(sNext);
                next = true;
            } else if (sNew.trim().endsWith(":") && !sNew.startsWith("  ")) {
                //CATEGORY это значить что он входит в новый уровень. значит пробелы увеличяться на 2 значения.
                // Получаем путь и отправляем в рекурсию.
                String _path = path + sNew.substring(0, sNew.length() - 1);
                if (_com.get(_path) != null) {
                    List<String> c = _com.get(_path);
                    c.forEach(s1 -> newComment.add(start + "# " + s1));
                }
                newComment.add(sNext);
                newComment.addAll(create(lvl + 1, list, _com, _path));
            } else if (sNew.startsWith("  ")) {
                // Тут я заметил очен удивительную особеность. Которая портила проверку на переносы
                // Иногда строка переносилась. Методом обытных тестов. я понял что переноситься если только один пролбел между словами.
                // для полноты я сделал проверку на наличии в начале 2 пробелов. но чтобы не спутать с уровнем я
                // добавил туда небольою проверку. уровень не может начинаться с 2 пробелов.
                // на устронение этго бага я поратил 2 дня. Да я не топ прогер чтобы сразу понят как делать
                // Если бы я был топ. то я бы написал свою библиотеку(обертку) конфигурации Yaml.
                // ну а покачто можно наслаждаться этим))
                newComment.set(newComment.size() - 1, newComment.get(newComment.size() - 1) + sNew.substring(1));
                //newComment.add(sNext);
                next = true;
            } else {
                //FIELD обычное поле. просто записываем
                String _path = path + sNew.substring(0, sNew.indexOf(":"));
                if (_com.get(_path) != null) {
                    List<String> l = _com.get(_path);
                    l.forEach(s1 -> newComment.add(start + "# " + s1));
                }
                newComment.add(sNext);
                next = true;
            }

        }
        // Конец файла. просто возращаем новый лист выхядя из всех рекурсий...
        return newComment;
    }


}
