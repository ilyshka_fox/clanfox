package ru.ilyshka_fox.clanfox.data;

import java.util.ArrayList;
import java.util.HashMap;

public enum ChatMode {
    all, onliGlobal, onliClan;

    private static HashMap<String, ChatMode> mode = new HashMap<>();

    public static void setChatMode(String name, ChatMode mode) {
        if (mode == ChatMode.all)
            ChatMode.mode.remove(name);
        else
            ChatMode.mode.put(name, mode);
    }

    public static ChatMode getChatMode(String name) {
        if (mode.containsKey(name))
            return mode.get(name);
        return all;
    }

    public static ArrayList<String> getOnliClan() {
        ArrayList<String> l = new ArrayList<>();
        mode.forEach((c, y) -> {
            if (y == onliClan)
                l.add(c);
        });

        return l;
    }

    public static ChatMode getChatMode(ClanPlayers cp) {
        if (mode.containsKey(cp.getNickName()))
            return mode.get(cp.getNickName());
        return all;
    }

}
