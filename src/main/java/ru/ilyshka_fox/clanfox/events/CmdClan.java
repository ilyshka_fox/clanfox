package ru.ilyshka_fox.clanfox.events;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.ilyshka_fox.clanfox.core.yamController.global.messages;
import ru.ilyshka_fox.clanfox.menus.defaylt.MainMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.msgMenu;
import ru.ilyshka_fox.clanfox.setting.Pex;

public class CmdClan implements CommandExecutor {
    public static boolean load = false;

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("clanfox")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Only players can use this command!");
                return true;
            }
            Player p = (Player) sender;
            if (load) {


                if (p.hasPermission(Pex.PEX_PLAYER_MENU))
                    MainMenu.open(p);
                else
                    p.sendMessage(messages.noPex);

            } else
                p.sendMessage(messages.load);
            return true;
        }
        return false;
    }
}
