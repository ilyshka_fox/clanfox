package ru.ilyshka_fox.clanfox.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import org.bukkit.event.player.PlayerQuitEvent;
import ru.ilyshka_fox.clanfox.Main;
import ru.ilyshka_fox.clanfox.core.NMS.PlayerNMS;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;

public class Join implements Listener {

    public static void updateAccaunt(Player p) {
        try {
            String s = PlayerNMS.getSkinProfil(p);
            if (!dbManager.hasAccount(p.getName())) {
                if (s == null)
                    Main.sql.execute(Main.QSQL.createAccountNoSkin, p.getName());
                else
                    Main.sql.execute(Main.QSQL.createAccount, p.getName(), s);
            } else
                Main.sql.execute(Main.QSQL.updateAccountSkin, s, p.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateExit(Player p) {
        try {
            Main.sql.execute(Main.QSQL.updateAccountExit, System.currentTimeMillis(), p.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        updateAccaunt(e.getPlayer());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        updateExit(e.getPlayer());
    }
}
