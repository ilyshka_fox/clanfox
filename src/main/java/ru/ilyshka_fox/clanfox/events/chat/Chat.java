package ru.ilyshka_fox.clanfox.events.chat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import ru.ilyshka_fox.clanfox.Data;
import ru.ilyshka_fox.clanfox.Main;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;
import ru.ilyshka_fox.clanfox.core.yamController.global.messages;
import ru.ilyshka_fox.clanfox.data.ChatMode;
import ru.ilyshka_fox.clanfox.data.ClanPlayers;
import ru.ilyshka_fox.clanfox.menus.defaylt.ErrorMenu;
import ru.ilyshka_fox.clanfox.setting.Config;
import ru.ilyshka_fox.clanfox.setting.Pex;

import java.util.ArrayList;
import java.util.List;

public class Chat implements Listener {

    public static void close() {
        sendMail.close();
        reDescriptionClan.close();
    }

//    @EventHandler(priority = EventPriority.HIGHEST)
//    public void AsyncPlayerChatEvent4(AsyncPlayerChatEvent e) {
//        if (e.getFormat().contains("!clantag!") && !e.isCancelled()) {
//            int clans = 0;
//            try {
//                clans = (int) Main.sql.executeCunsQuery(resSet -> {
//                    if (resSet.next()) {
//                        return resSet.getInt(1);
//                    }
//                    return 0;
//                }, "SELECT [ClanID] FROM [Players] join [Data] on [Data].[PlayerID] = [Players].[ID] where [Players].[name] = ?", e.getPlayer().getName());
//            } catch (Exception e1) {
//                e1.printStackTrace();
//            }
//
//            String format = Config.tegFormat.replaceFirst("<tag>", Data.getTeg(clans));
//            e.setFormat(e.getFormat().replaceFirst("!clantag!", format));
//        }
//    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void AsyncPlayerChatEvent(AsyncPlayerChatEvent e) {
        if (!e.isCancelled()) {
            final Player p = e.getPlayer();
            try {
                final ClanPlayers cp = dbManager.getClanPlayers(p.getName());

                reDescriptionClan.AsyncPlayerChatEvent(e);
                if (e.isCancelled()) return;
                sendMail.AsyncPlayerChatEvent(e, cp.getClanid());
                if (e.isCancelled()) return;

                if (cp.getClanid() > 0) {
                    if (ChatMode.getChatMode(p.getName()) == ChatMode.onliClan || ((e.getMessage().startsWith(Config.clanChatChar) && ChatMode.getChatMode(p.getName()) == ChatMode.all && e.getMessage().length() > 1))) {
                        String msg = Config.clanChatFormat
                                .replace("<tag>", Config.tegFormat.replaceFirst("<tag>", Data.getTeg(cp.getClanid())))
                                .replaceFirst("<player>", p.getName())
                                .replaceFirst("<msg>", (e.getMessage().startsWith(Config.clanChatChar) ?
                                        e.getMessage().substring(1, e.getMessage().length()).trim() : e.getMessage()));
                        String adm = Config.clanChatAdmin
                                .replace("<tag>", Config.tegFormat.replaceFirst("<tag>", Data.getTeg(cp.getClanid())))
                                .replaceFirst("<player>", p.getName())
                                .replaceFirst("<msg>", (e.getMessage().startsWith(Config.clanChatChar) ?
                                        e.getMessage().substring(1, e.getMessage().length()).trim() : e.getMessage()));


                        Main.sql.executeQuery(resSer -> {
                            List<String> listClan = new ArrayList<>();
                            while (resSer.next()) {
                                listClan.add(resSer.getString("name"));
                            }
                            List<Player> listOnline = new ArrayList<>(Bukkit.getOnlinePlayers());

                            listOnline.forEach(player -> {
                                if (listClan.contains(player.getName())) {
                                    if (ChatMode.getChatMode(player.getName()) != ChatMode.onliGlobal)
                                        Bukkit.getScheduler().runTask(Main.plugin, () -> player.sendMessage(msg));
                                } else if (player.hasPermission(Pex.PEX_ADMIN_CHAT)) {
                                    if (ChatMode.getChatMode(player.getName()) == ChatMode.all)
                                        Bukkit.getScheduler().runTask(Main.plugin, () -> player.sendMessage(adm));
                                }

                            });
                            return null;
                        }, Main.QSQL.getClanMemberName, cp.getClanid());

                        e.setCancelled(true);
                        return;
                    }
                }

                for (String nam : ChatMode.getOnliClan()) {
                    Player p2 = Bukkit.getPlayer(nam);
                    if (p2 != null)
                        if (e.getRecipients().contains(p2.getPlayer()))
                            e.getRecipients().remove(p2.getPlayer());
                }
                String format = cp.getClanid() > 0 ? Config.tegFormat.replaceFirst("<tag>", Data.getTeg(cp.getClanid())) : "";
                e.setFormat(e.getFormat().replace("!clantag!", format));
            } catch (Exception e1) {
                e1.printStackTrace();
                ErrorMenu.open(p);
            }
        }
    }
}
