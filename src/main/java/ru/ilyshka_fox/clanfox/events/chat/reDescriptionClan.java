package ru.ilyshka_fox.clanfox.events.chat;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import ru.ilyshka_fox.clanfox.Main;
import ru.ilyshka_fox.clanfox.data.Clan;
import ru.ilyshka_fox.clanfox.menus.MenuEx;
import ru.ilyshka_fox.clanfox.menus.Question.QReDescriptionClan;
import ru.ilyshka_fox.clanfox.menus.defaylt.ErrorMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.LoadMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.MainMenu;

public class reDescriptionClan {

    // Масив игроков которые хотят изменить описание.
    private static Map<String, Clan> reopisAdminClan = new HashMap<>();
    private static Map<String, Runnable> reopisAdminRunnable = new HashMap<>();

    public static void addReDiscription(Player p, Clan c, Runnable open) {
        reopisAdminClan.put(p.getName(), c);
        reopisAdminRunnable.put(p.getName(), open);
    }

    public static void AsyncPlayerChatEvent(AsyncPlayerChatEvent e) {
        if (reopisAdminClan.containsKey(e.getPlayer().getName())) {
            e.setCancelled(true);
            LoadMenu.open(e.getPlayer());
            final Player p = e.getPlayer();
            final String msg = e.getMessage();
            MenuEx.runAcuns(() -> {
                try {
                    QReDescriptionClan.open(p, reopisAdminClan.get(p.getName()), msg, reopisAdminRunnable.get(p.getName()));
                    reopisAdminClan.remove(p.getName());
                    reopisAdminRunnable.remove(p.getName());
                } catch (Exception e1) {
                    e1.printStackTrace();
                    ErrorMenu.open(p);
                }
            });

        }
    }

    public static boolean contact(Player p) {
        return (reopisAdminClan.containsKey(p.getName()));
    }

    public static Runnable getRun(Player p) {
        if (reopisAdminRunnable.containsKey(p.getName())) {
            return reopisAdminRunnable.get(p.getName());
        } else {
            return () -> MainMenu.open(p);
        }
    }

    public static boolean remove(Player p) {
        if (reopisAdminClan.containsKey(p.getName())) {
            reopisAdminClan.remove(p.getName());
            reopisAdminRunnable.remove(p.getName());
            return true;
        } else
            return false;
    }

    public static void close() {
        reopisAdminClan.clear();
        reopisAdminClan = null;
        reopisAdminRunnable.clear();
        reopisAdminRunnable = null;
    }
}
