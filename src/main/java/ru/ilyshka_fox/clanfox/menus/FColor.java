package ru.ilyshka_fox.clanfox.menus;

import org.bukkit.ChatColor;

public enum FColor {
    TITLE       (ChatColor.DARK_GRAY),
    BTN_NAME    (ChatColor.BLUE),
    BTN_BLACK   (ChatColor.DARK_RED),
    LORE        (ChatColor.WHITE),
    INFO        (ChatColor.GREEN),
    ERROR       (ChatColor.RED + "" + ChatColor.BOLD);

    String color;

    FColor(String s) {
        this.color = s;
    }

    FColor(ChatColor s) {
        this.color = s.toString();
    }

    @Override
    public String toString() {
        return color;
    }
}
