package ru.ilyshka_fox.clanfox.menus;

public enum FSlot {
    Yes(2),
    No(6);

    Integer slot;

    FSlot(int s) {
        this.slot = s;
    }

    @Override
    public String toString() {
        return slot.toString();
    }

    public Integer toInt() {
        return slot;
    }
}
