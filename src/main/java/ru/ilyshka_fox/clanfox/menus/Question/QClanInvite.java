package ru.ilyshka_fox.clanfox.menus.Question;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.ilyshka_fox.clanfox.core.menu.GUIHolder;
import ru.ilyshka_fox.clanfox.core.menu.Item_builder;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;
import ru.ilyshka_fox.clanfox.core.yamController.Contain;
import ru.ilyshka_fox.clanfox.core.yamController.Embedded;
import ru.ilyshka_fox.clanfox.core.yamController.Value;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;
import ru.ilyshka_fox.clanfox.core.yamController.global.messages;
import ru.ilyshka_fox.clanfox.data.Clan;
import ru.ilyshka_fox.clanfox.data.ClanPlayers;
import ru.ilyshka_fox.clanfox.menus.FColor;
import ru.ilyshka_fox.clanfox.menus.FSlot;
import ru.ilyshka_fox.clanfox.menus.MenuEx;
import ru.ilyshka_fox.clanfox.menus.defaylt.ErrorMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.LoadMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.MyClanMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.MyClanMenu.MyClanMenuType;
import ru.ilyshka_fox.clanfox.menus.defaylt.msgMenu;
import ru.ilyshka_fox.clanfox.setting.Pex;

import java.util.ArrayList;
import java.util.Arrays;

@Contain(config = "Setting", path = "question.clanInvite")
public class QClanInvite extends MenuEx {


    @Value(comment = "<nickname>")
    public static String title = FColor.TITLE + "Вы хотите пригласить игрока " + FColor.INFO + "<nickname>" + FColor.TITLE + "?";

    @Embedded(comment = "Информация игрока")
    public static itemHead info = new itemHead(FColor.BTN_NAME + "Информация", Arrays.asList(
            FColor.LORE + "Ник: " + FColor.INFO + "<nickname>",
            FColor.LORE + "Убийств: " + FColor.INFO + "<kills>",
            FColor.LORE + "Смертей: " + FColor.INFO + "<death>",
            FColor.LORE + "Репутация: " + FColor.INFO + "<KD>"
    ), "PAPER", "0", 4, null);
    @Embedded
    public static itemHead yes = new itemHead(FColor.BTN_NAME + "Да", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", FSlot.Yes.toInt(), null);
    @Embedded
    public static itemHead no = new itemHead(FColor.BTN_NAME + "Нет", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", FSlot.No.toInt(), null);


    public static void open(final Player p, ClanPlayers cp, ClanPlayers player, Clan c) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {
                GUIHolder g = new GUIHolder(1);

                Item_builder btnInfo = new Item_builder(info, s -> player.replase(s)).localizedName("info");
                Item_builder btnYes = new Item_builder(yes, s -> player.replase(s)).localizedName("yes").ItemListener((x, y) -> {
                    if (!p.hasPermission(Pex.PEX_PLAYER_INVITE)) {
                        msgMenu.open(p, messages.noPex, () -> MyClanMenu.open(p, MyClanMenuType.add, 1));
                        return;
                    }

                    try {
                        dbManager.setClanInvate(c.getId(), player.getId());
                        Player p2 = Bukkit.getPlayer(player.getNickName());
                        if (p2 != null)
                            p2.sendMessage(c.replase(messages.clanInvitation));

                        p.sendMessage(messages.clanSendInvitation.replace("<nickname>", player.getNickName()));
                        MyClanMenu.open(p, MyClanMenuType.add, 1);
                    } catch (Exception e) {
                        e.printStackTrace();
                        ErrorMenu.open(p);
                    }
                });
                Item_builder btnNo = new Item_builder(no, s -> player.replase(s)).localizedName("no").ItemListener((x, y) -> MyClanMenu.open(p, MyClanMenuType.add, 1));
                g.setTitle(player.replase(title));
                g.setButton(info.getSlot(), btnInfo);
                g.setButton(yes.getSlot(), btnYes);
                g.setButton(no.getSlot(), btnNo);
                g.open(p);
            } catch (Exception e) {
                ErrorMenu.open(p);
                e.printStackTrace();
            }
        });

    }
}
