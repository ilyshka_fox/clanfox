package ru.ilyshka_fox.clanfox.menus.Question;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.ilyshka_fox.clanfox.core.menu.GUIHolder;
import ru.ilyshka_fox.clanfox.core.menu.Item_builder;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;
import ru.ilyshka_fox.clanfox.core.yamController.Contain;
import ru.ilyshka_fox.clanfox.core.yamController.Embedded;
import ru.ilyshka_fox.clanfox.core.yamController.Value;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;
import ru.ilyshka_fox.clanfox.core.yamController.global.messages;
import ru.ilyshka_fox.clanfox.data.ChatMode;
import ru.ilyshka_fox.clanfox.data.Clan;
import ru.ilyshka_fox.clanfox.menus.FColor;
import ru.ilyshka_fox.clanfox.menus.FSlot;
import ru.ilyshka_fox.clanfox.menus.MenuEx;
import ru.ilyshka_fox.clanfox.menus.defaylt.ErrorMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.LoadMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.MainMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.msgMenu;
import ru.ilyshka_fox.clanfox.setting.Pex;

import java.util.ArrayList;
import java.util.Arrays;

@Contain(config = "Setting", path = "question.disbandClan")
public class QDisbandClan extends MenuEx {

    @Value(comment = "<nickname>")
    public static String title = FColor.TITLE + "Вы хотите распустить клан?";

    @Embedded(comment = "Информация клана")
    public static itemHead info = new itemHead(FColor.BTN_NAME + "Информация", Arrays.asList(
            FColor.LORE + "Клан нельзя будет восстановить!!"
    ), "PAPER", "0", 4, null);

    @Embedded
    public static itemHead yes = new itemHead(FColor.BTN_NAME + "Да", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", FSlot.Yes.toInt(), null);
    @Embedded
    public static itemHead no = new itemHead(FColor.BTN_NAME + "Нет", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", FSlot.No.toInt(), null);


    public static void open(final Player p, final Clan c, Runnable open) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {

                GUIHolder g = new GUIHolder(1);

                Item_builder btnInfo = new Item_builder(info, c::replase).localizedName("info");
                Item_builder btnYes = new Item_builder(yes, c::replase).localizedName("yes").ItemListener((x, y) -> {
                    if (!p.hasPermission(Pex.PEX_PLAYER_DELETE) && !p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
                        msgMenu.open(p, messages.noPex, open);
                        return;
                    }

                    LoadMenu.open(p);
                    runAcuns(() -> {
                        try {
                            c.getMembers().forEach(mem -> {
                                Player pmem = Bukkit.getPlayer(mem.getNickName());
                                if (pmem != null) pmem.sendMessage(messages.disbandClan);
                                ChatMode.setChatMode(mem.getNickName(), ChatMode.all);
                            });
                            dbManager.DesbandClan(c.getId());
                            MainMenu.open(p);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            ErrorMenu.open(p);
                        }
                    });
                });
                Item_builder btnNo = new Item_builder(no, c::replase).localizedName("no").ItemListener((x, y) -> open.run());
                g.setTitle(c.replase(title));
                g.setButton(info.getSlot(), btnInfo);
                g.setButton(yes.getSlot(), btnYes);
                g.setButton(no.getSlot(), btnNo);
                g.open(p);
            } catch (Exception e) {
                ErrorMenu.open(p);
                e.printStackTrace();
            }
        });

    }
}
