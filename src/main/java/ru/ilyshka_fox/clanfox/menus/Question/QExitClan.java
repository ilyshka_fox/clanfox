package ru.ilyshka_fox.clanfox.menus.Question;

import org.bukkit.entity.Player;
import ru.ilyshka_fox.clanfox.Main;
import ru.ilyshka_fox.clanfox.core.menu.GUIHolder;
import ru.ilyshka_fox.clanfox.core.menu.Item_builder;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;
import ru.ilyshka_fox.clanfox.core.yamController.Contain;
import ru.ilyshka_fox.clanfox.core.yamController.Embedded;
import ru.ilyshka_fox.clanfox.core.yamController.Value;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;
import ru.ilyshka_fox.clanfox.core.yamController.global.messages;
import ru.ilyshka_fox.clanfox.data.ChatMode;
import ru.ilyshka_fox.clanfox.data.Clan;
import ru.ilyshka_fox.clanfox.menus.FColor;
import ru.ilyshka_fox.clanfox.menus.FSlot;
import ru.ilyshka_fox.clanfox.menus.MenuEx;
import ru.ilyshka_fox.clanfox.menus.defaylt.*;
import ru.ilyshka_fox.clanfox.setting.Pex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@Contain(config = "Setting", path = "question.exitClan")
public class QExitClan extends MenuEx {

    @Value(comment = "Информация клана")
    public static String title = FColor.TITLE + "Вы хотите покинуть клан?";

    @Embedded(comment = "Информация клана")
    public static itemHead info = new itemHead(FColor.BTN_NAME + "Информация", Collections.singletonList(
            FColor.LORE + "Клан: " + FColor.INFO + "<clanname>"
    ), "PAPER", "0", 4, null);
    @Embedded
    public static itemHead yes = new itemHead(FColor.BTN_NAME + "Да", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", FSlot.Yes.toInt(), null);
    @Embedded
    public static itemHead no = new itemHead(FColor.BTN_NAME + "Нет", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", FSlot.No.toInt(), null);


    public static void open(final Player p, final Clan c) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {
                GUIHolder g = new GUIHolder(1);
                Item_builder btnInfo = new Item_builder(info, (s) -> c.replase(s)).localizedName("info");
                Item_builder btnYes = new Item_builder(yes, (s) -> c.replase(s)).localizedName("yes").ItemListener((x, y) -> {
                    if (!p.hasPermission(Pex.PEX_PLAYER_EXIT)) {
                        msgMenu.open(p, messages.noPex, () -> MainMenu.open(p));
                        return;
                    }
                    LoadMenu.open(p);
                    runAcuns(() -> {
                        try {
                            ChatMode.setChatMode(p.getName(), ChatMode.all);
                            dbManager.removeCash(p.getName());
                            dbManager.exitClan(p.getName());
                            MainMenu.open(p);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            ErrorMenu.open(p);
                        }
                    });
                });
                Item_builder btnNo = new Item_builder(no, (s) -> c.replase(s)).localizedName("no").ItemListener((x, y) -> MainMenu.open(p));
                g.setTitle(c.replase(title));
                g.setButton(info.getSlot(), btnInfo);
                g.setButton(yes.getSlot(), btnYes);
                g.setButton(no.getSlot(), btnNo);
                g.open(p);
            } catch (Exception e) {
                ErrorMenu.open(p);
                e.printStackTrace();
            }
        });

    }
}
