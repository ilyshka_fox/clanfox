package ru.ilyshka_fox.clanfox.menus.anvil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import ru.ilyshka_fox.clanfox.Main;
import ru.ilyshka_fox.clanfox.core.menu.Item_builder;
import ru.ilyshka_fox.clanfox.core.menu.anvil.AnvilHolder;
import ru.ilyshka_fox.clanfox.core.menu.anvil.AnvilLisenger;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;
import ru.ilyshka_fox.clanfox.core.yamController.Contain;
import ru.ilyshka_fox.clanfox.core.yamController.Embedded;
import ru.ilyshka_fox.clanfox.core.yamController.Value;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;
import ru.ilyshka_fox.clanfox.data.ClanPlayers;
import ru.ilyshka_fox.clanfox.menus.FColor;
import ru.ilyshka_fox.clanfox.menus.MenuEx;
import ru.ilyshka_fox.clanfox.menus.Question.QCreateClan;
import ru.ilyshka_fox.clanfox.menus.defaylt.ErrorMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.MainMenu;
import ru.ilyshka_fox.clanfox.setting.Config;
import ru.ilyshka_fox.clanfox.setting.Pex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Contain(config = "Setting", path = "anvil.createClan")
public class CreateClan extends MenuEx {

    @Embedded
    private static itemHead info = new itemHead("Введите имя клана", Collections.emptyList(), "NAME_TAG", "0", null, null);
    @Embedded
    private static itemHead cancel = new itemHead(FColor.BTN_BLACK + "Назад", Collections.emptyList(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", null, null);
    @Embedded
    private static itemHead next = new itemHead(ChatColor.RESET.toString(), Collections.singletonList(FColor.LORE + "Создать клан " + FColor.INFO + "<colortext>"), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", null, null);

    @Embedded
    private static itemHead errors = new itemHead(ChatColor.RESET.toString(), Collections.emptyList(), "BARRIER", "0", null, null);

    @Value(path = "errors.errors.regExName")
    private static List<String> regExName = Collections.singletonList(FColor.ERROR + "В имене клана запрещенные символы");
    @Value(path = "errors.errors.minName")
    private static List<String> minName = Collections.singletonList(FColor.ERROR + "Имя клана слишком маленькое");
    @Value(path = "errors.errors.maxName")
    private static List<String> maxName = Collections.singletonList(FColor.ERROR + "Имя клана слишком большое");
    @Value(path = "errors.errors.maxColorCode")
    private static List<String> maxColorCode = Collections.singletonList(FColor.ERROR + "Превышино максимальное количество цветовых кодов");
    @Value(path = "errors.errors.magicColor")
    private static List<String> magicColor = Collections.singletonList(FColor.ERROR + "Запрещено использовать магический шрифт");
    @Value(path = "errors.errors.banName")
    private static List<String> banName = Collections.singletonList(FColor.ERROR + "Вы используете запрещенные слова или части слова");
    @Value(path = "errors.errors.nameInUse")
    private static List<String> nameInUse = Collections.singletonList(FColor.ERROR + "Данное имя занято");


    public static void open(Player p, ClanPlayers cp) {
        final Item_builder infoBut = new Item_builder(info)
                .localizedName(ChatColor.RESET.toString());
        final Item_builder cancelBut = new Item_builder(cancel)
                .localizedName(ChatColor.RESET.toString());
        final Item_builder nextBut = new Item_builder(next)
                .localizedName(ChatColor.RESET.toString());
        final Item_builder errorsBut = new Item_builder(errors)
                .localizedName(ChatColor.RESET.toString());

        AnvilHolder.open(p, new AnvilLisenger() {
            ArrayList<String> zanname = new ArrayList<>();
            String name = " ";
            boolean ok = false;

            @Override
            public void onPrepareAnvil(PrepareAnvilEvent e) {
                String name = (e.getInventory().getRenameText().trim().equalsIgnoreCase("")) ? " "
                        : e.getInventory().getRenameText().trim();
                String colornamr = ChatColor.translateAlternateColorCodes('&', name);
                String nocolorname = ChatColor.stripColor(colornamr);
                ok = false;


                if (nocolorname.length() < Config.minName) {
                    errorsBut.setLore(minName);
                    e.setResult(errorsBut.build());
                } else if (nocolorname.length() > Config.maxName) {
                    errorsBut.setLore(maxName);
                    e.setResult(errorsBut.build());
                } else if (!name.matches(Config.regExName)) {
                    errorsBut.setLore(regExName);
                    e.setResult(errorsBut.build());
                } else if (((colornamr.length() - nocolorname.length()) / 2 > 0
                                && !p.hasPermission(Pex.PEX_GROUP_COLOR))
                        && ((colornamr.length() - nocolorname.length()) / 2 > Config.maxColorCode
                                && !p.hasPermission(Pex.PEX_GROUP_COLOR_NO_MAX))) {
                    errorsBut.setLore(maxColorCode);
                    e.setResult(errorsBut.build());
                } else if (name.matches("(.*)&[Kk](.*)") && !p.hasPermission(Pex.PEX_GROUP_COLOR_MAGIC)) {
                    errorsBut.setLore(magicColor);
                    e.setResult(errorsBut.build());
                } else {
                    boolean ban = false;
                    for (String s : Config.BAN_NAME) {
                        if (nocolorname.toLowerCase().contains(s.toLowerCase())) {
                            errorsBut.setLore(banName);
                            e.setResult(errorsBut.build());
                            ban = true;
                            break;
                        }
                    }
                    for (String s : zanname) {
                        if (nocolorname.equalsIgnoreCase(s)) {
                            errorsBut.setLore(nameInUse);
                            e.setResult(errorsBut.build());
                            ban = true;
                            break;
                        }
                    }

                    if (!ban) {
                        if (nocolorname.equals("IlyshkaFox")) {
                            nextBut.material(Material.SKULL_ITEM);
                            nextBut.configData(
                                    "eyJ0aW1lc3RhbXAiOjE0ODM2Mjk4MzE3MzQsInByb2ZpbGVJZCI6IjRmOThlOWFjZjAxODQ1MzRiNWU2YmRkMzJjYmI3MGNlIiwicHJvZmlsZU5hbWUiOiJpbHlzaGthIiwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2UyMWNhOTk4NDQ4MDNjMjNkMjEzYWY0NWNjMGY2NGVlODExZDY2ZjcxZDdkZTVhZWM2MTNjNzM2ZTNkIn19fQ==");
                        }
                        List<String> lore = next.getLore();
                        lore.replaceAll(t -> t.replaceFirst("<text>", name).replaceFirst("<colortext>", colornamr)
                                .replaceFirst("<nocolortext>", nocolorname));
                        e.setResult(nextBut.setLore(lore).name(next.getName().replaceFirst("<text>", name)
                                .replaceFirst("<colortext>", colornamr).replaceFirst("<nocolortext>", nocolorname))
                                .build());
                        this.name = colornamr;
                        ok = true;
                    }
                }
            }

            @Override
            public void onInventoryClick(InventoryClickEvent e) {
                final String name = this.name;
                switch (e.getRawSlot()) {
                    case 1:
                        MainMenu.open(p);
                        break;
                    case 2:
                        if (ok) {
                            runAcuns(() -> {
                                try {
                                    if (!dbManager.hasClan(name)) {
                                        if (Config.nameToTag) {
                                            Bukkit.getScheduler().runTask(Main.plugin, () -> QCreateClan.open(p, name));
                                        } else {
                                            Bukkit.getScheduler().runTask(Main.plugin, () -> CreateTag.open(p, cp, name));
                                        }
                                    } else {
                                        zanname.add(ChatColor.stripColor(name));
                                        e.getInventory().setItem(2,
                                                errorsBut.setLore(nameInUse).build());
                                    }
                                } catch (Exception e1) {
                                    ErrorMenu.open(p);
                                    e1.printStackTrace();
                                }
                            });
                        }
                        break;
                    default:
                        break;
                }

            }

            @Override
            public void close() {
            }

            @Override
            public void open(AnvilInventory inv) {
                inv.setItem(0, infoBut.build());
                inv.setItem(1, cancelBut.build());
            }
        });
    }
}
