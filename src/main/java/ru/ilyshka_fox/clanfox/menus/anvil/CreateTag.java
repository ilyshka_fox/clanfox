package ru.ilyshka_fox.clanfox.menus.anvil;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import ru.ilyshka_fox.clanfox.core.menu.Item_builder;
import ru.ilyshka_fox.clanfox.core.menu.anvil.AnvilHolder;
import ru.ilyshka_fox.clanfox.core.menu.anvil.AnvilLisenger;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;
import ru.ilyshka_fox.clanfox.core.yamController.Contain;
import ru.ilyshka_fox.clanfox.core.yamController.Embedded;
import ru.ilyshka_fox.clanfox.core.yamController.Value;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;
import ru.ilyshka_fox.clanfox.data.ClanPlayers;
import ru.ilyshka_fox.clanfox.menus.FColor;
import ru.ilyshka_fox.clanfox.menus.MenuEx;
import ru.ilyshka_fox.clanfox.menus.Question.QCreateClan;
import ru.ilyshka_fox.clanfox.menus.defaylt.ErrorMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.MainMenu;
import ru.ilyshka_fox.clanfox.setting.Config;
import ru.ilyshka_fox.clanfox.setting.Pex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Contain(config = "Setting", path = "anvil.createTag")
public class CreateTag extends MenuEx {

    @Embedded
    public static itemHead info = new itemHead("Введите тег клана", Collections.emptyList(), "NAME_TAG", "0", null, null);
    @Embedded
    public static itemHead cancel = new itemHead(FColor.BTN_BLACK + "Назад", Collections.emptyList(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", null, null);
    @Embedded
    public static itemHead next = new itemHead(ChatColor.RESET.toString(), Arrays.asList(FColor.LORE + "Использовать тег " + FColor.INFO + "<colortext>", "Продолжить"), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", null, null);

    @Embedded
    public static itemHead errors = new itemHead(ChatColor.RESET.toString(), Collections.emptyList(), "BARRIER", "0", null, null);

    @Value(path = "errors.errors.regExName")
    public static List<String> regExName = Collections.singletonList(FColor.ERROR + "В теге клана запрещенные символы");
    @Value(path = "errors.errors.sizeName")
    public static List<String> sizeName = Collections.singletonList(FColor.ERROR + "Тег клана должен состоять из 3 символов");
    @Value(path = "errors.errors.maxColorCode")
    public static List<String> maxColorCode = Collections.singletonList(FColor.ERROR + "Превышино максимальное количество цветовых кодов");
    @Value(path = "errors.errors.magikColor")
    public static List<String> magikColor = Collections.singletonList(FColor.ERROR + "Запрещено использовать магический шрифт");
    @Value(path = "errors.errors.banName")
    public static List<String> banName = Collections.singletonList(FColor.ERROR + "Вы используете запрещенные слова или части слова");
    @Value(path = "errors.errors.nameInUse")
    public static List<String> nameInUse = Collections.singletonList(FColor.ERROR + "Данноый тег занят");


    public static void open(Player p, ClanPlayers cp, String clanname) {
        final Item_builder infoBnt = new Item_builder(info)
                .localizedName(ChatColor.RESET.toString());
        final Item_builder cancelBnt = new Item_builder(cancel)
                .localizedName(ChatColor.RESET.toString());
        final Item_builder nextBnt = new Item_builder(next)
                .localizedName(ChatColor.RESET.toString());
        final Item_builder errorsBnt = new Item_builder(errors)
                .localizedName(ChatColor.RESET.toString());

        AnvilHolder.open(p, new AnvilLisenger() {
            ArrayList<String> zanname = new ArrayList<>();
            String name = " ";
            boolean ok = false;

            @Override
            public void onPrepareAnvil(PrepareAnvilEvent e) {
                String name = (e.getInventory().getRenameText().trim() == null) ? " "
                        : e.getInventory().getRenameText().trim();
                String colornamr = ChatColor.translateAlternateColorCodes('&', name);
                String nocolorname = ChatColor.stripColor(colornamr);
                ok = false;
                if (nocolorname.length() != Config.tagSize) {
                    errorsBnt.setLore(sizeName);
                    e.setResult(errorsBnt.build());
                } else if (!name.matches(Config.regExName)) {
                    errorsBnt.setLore(regExName);
                    e.setResult(errorsBnt.build());
                } else if (((colornamr.length() - nocolorname.length()) / 2 > 0
                        && !p.hasPermission(Pex.PEX_GROUP_COLOR))
                        && ((colornamr.length() - nocolorname.length()) / 2 > Config.maxColorCode
                        && !p.hasPermission(Pex.PEX_GROUP_COLOR_NO_MAX)))  {
                    errorsBnt.setLore(maxColorCode);
                    e.setResult(errorsBnt.build());
                } else if (name.matches("(.*)&[Kk](.*)") && !p.hasPermission(Pex.PEX_GROUP_COLOR_MAGIC)) {
                    errorsBnt.setLore(magikColor);
                    e.setResult(errorsBnt.build());
                } else {
                    boolean ban = false;
                    for (String s : Config.BAN_NAME) {
                        if (nocolorname.toLowerCase().contains(s.toLowerCase())) {
                            errorsBnt.setLore(banName);
                            e.setResult(errorsBnt.build());
                            ban = true;
                            break;
                        }
                    }
                    for (String s : zanname) {
                        if (nocolorname.equalsIgnoreCase(s)) {
                            errorsBnt.setLore(nameInUse);
                            e.setResult(errorsBnt.build());
                            ban = true;
                            break;
                        }
                    }
                    if (!ban) {
                        List<String> lore = next.getLore();
                        lore.replaceAll(t -> t.replaceFirst("<text>", name).replaceFirst("<colortext>", colornamr)
                                .replaceFirst("<nocolortext>", nocolorname));
                        e.setResult(nextBnt.setLore(lore).name(next.getName().replaceFirst("<text>", name)
                                .replaceFirst("<colortext>", colornamr).replaceFirst("<nocolortext>", nocolorname))
                                .build());
                        this.name = colornamr;
                        ok = true;
                    }
                }
            }

            @Override
            public void onInventoryClick(InventoryClickEvent e) {
                final String name = this.name;
                switch (e.getRawSlot()) {
                    case 0:
                        break;
                    case 1:
                        MainMenu.open(p);
                        break;
                    case 2:
                        if (ok) {
                            try {
                                if (!dbManager.hasTag(name)) {
                                    QCreateClan.open(p, clanname, name);
                                } else {
                                    zanname.add(ChatColor.stripColor(name));
                                    e.getInventory().setItem(2, errorsBnt.setLore(nameInUse).build());
                                }
                            } catch (Exception e1) {
                                ErrorMenu.open(p);
                                e1.printStackTrace();
                            }
                        }
                        break;
                    default:
                        break;
                }

            }

            @Override
            public void close() {
            }

            @Override
            public void open(AnvilInventory inv) {
                inv.setItem(0, infoBnt.build());
                inv.setItem(1, cancelBnt.build());
            }
        });

    }
}
