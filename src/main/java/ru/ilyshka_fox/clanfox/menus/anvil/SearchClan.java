package ru.ilyshka_fox.clanfox.menus.anvil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import ru.ilyshka_fox.clanfox.core.menu.Item_builder;
import ru.ilyshka_fox.clanfox.core.menu.anvil.AnvilHolder;
import ru.ilyshka_fox.clanfox.core.menu.anvil.AnvilLisenger;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;
import ru.ilyshka_fox.clanfox.core.yamController.Contain;
import ru.ilyshka_fox.clanfox.core.yamController.Embedded;
import ru.ilyshka_fox.clanfox.core.yamController.Value;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;
import ru.ilyshka_fox.clanfox.data.Clan;
import ru.ilyshka_fox.clanfox.data.ClanPlayers;
import ru.ilyshka_fox.clanfox.menus.FColor;
import ru.ilyshka_fox.clanfox.menus.MenuEx;
import ru.ilyshka_fox.clanfox.menus.Question.QAcceptRequest;
import ru.ilyshka_fox.clanfox.menus.Question.QClanInvite;
import ru.ilyshka_fox.clanfox.menus.defaylt.ErrorMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.MainMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.MyClanMenu;
import ru.ilyshka_fox.clanfox.menus.defaylt.MyClanMenu.MyClanMenuType;
import ru.ilyshka_fox.clanfox.menus.defaylt.OtherClanMenu;

import java.util.*;

@Contain(config = "Setting", path = "anvil.searchClan")
public class SearchClan extends MenuEx {

    @Embedded
    public static itemHead info = new itemHead(" ", Arrays.asList(FColor.LORE + "Поиск кланов", FColor.LORE + "# - поиск по идентификаторку клана"), "NAME_TAG", "0", null, null);
    @Embedded
    public static itemHead cancel = new itemHead(FColor.BTN_BLACK + "Назад", Collections.emptyList(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", null, null);

    @Embedded
    private static itemHead next = new itemHead(ChatColor.RESET.toString(),
            null,
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", null, null);

    @Embedded
    private static itemHead errors = new itemHead(
            ChatColor.RESET.toString(),
            Collections.emptyList(),
            "BARRIER", "0", null, null);


    @Value(path = "errors.good.searchClanPlayer", comment = "<clan>")
    private static List<String> searchClan = Collections.singletonList(FColor.LORE + "Найден клан: " + FColor.INFO + "<clan>");
    //    @Value(path = "errors.good.searchClanPlayer", comment = "<player> <clan>")
//    private static List<String> searchClanPlayer = Arrays.asList(
//            FColor.LORE + "Найден игрок " + FColor.INFO + "<player>",
//            FColor.LORE + "Клан: " + FColor.INFO + "<clan>"
//    );
//    @Value(path = "errors.good.searchTag", comment = "<teg> <clan>")
//    private static List<String> searchTag = Arrays.asList(
//            FColor.LORE + "Поиск по тегу:",
//            FColor.LORE + "Найден клан: " + FColor.INFO + "<clan>"
//    );
    @Value(path = "errors.good.searchClanID", comment = "<clan>")
    private static List<String> searchClanID = Collections.singletonList(FColor.LORE + "Найден клан: " + FColor.INFO + "<clan>");


    @Value(path = "errors.error.searchClanPlayer")
    private static List<String> errSearchClan = Collections.singletonList(FColor.ERROR + "Клан не найден");
//    @Value(path = "errors.error.searchClanPlayer")
//    private static List<String> errSearchClanPlayer = Collections.singletonList(FColor.ERROR + "Игрок не найден или не состоит в клане");
//    @Value(path = "errors.error.searchTag", comment = "<teg>")
//    private static List<String> errSearchTag = Arrays.asList(FColor.ERROR + "Тег " + FColor.INFO + "<tag>" + FColor.ERROR + " не найден", "", FColor.LORE + "Введиет точный тег, с использование цветов");

    @Value(path = "errors.error.searchClanID")
    private static List<String> errSearchClanID = Collections.singletonList(FColor.ERROR + "Данный идентификатор клана не найден");
    @Value(path = "errors.error.searchIncorrectID")
    private static List<String> errSearchIncorrectID = Collections.singletonList(FColor.ERROR + "Данный идентификатор введен не корректно");
    @Value(path = "errors.error.empty")
    private static List<String> errEmpty = Collections.singletonList(FColor.ERROR + "Введите что нибудь в поиск");


    public static void open(Player p) {

        final Item_builder infoBnt = new Item_builder(info).localizedName("r");
        final Item_builder cancelBnt = new Item_builder(cancel).localizedName(ChatColor.RESET.toString());
        final Item_builder nextBnt = new Item_builder(next).localizedName(ChatColor.RESET.toString());
        final Item_builder errorsBut = new Item_builder(errors).localizedName(ChatColor.RESET.toString());
        List<Clan> clans;
        try {
            clans = dbManager.getTop();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorMenu.open(p);
            return;
        }


        AnvilHolder.open(p, new AnvilLisenger() {
            String name = " ";
            boolean ok = false;
            Clan clan = null;


            @Override
            public void onPrepareAnvil(PrepareAnvilEvent e) {
                ok = false;
                String na = (e.getInventory().getRenameText().trim().equals("")) ? " " : e.getInventory().getRenameText().trim();
                if (na.equals(" ") || na.length() == 0)
                    return;
                //Поиск
                // Происходит по имени клана
                // # поиск будет по id клану.

                if (na.trim().length() == 0 || (na.trim().length() == 1 && na.startsWith("#"))) {
                    errorsBut.setLore(errEmpty);
                    return;
                }

//                if (na.startsWith("!")) {
//                    String s = na.substring(1);
//                    Iterator<Clan> ic = clans.stream().filter(clan1 -> clan1.getMembers().stream().filter(clanPlayers -> clanPlayers.getNickName().equalsIgnoreCase(s)).count() > 0).limit(1).iterator();
//                    if (ic.hasNext()) {
//                        Clan c = ic.next();
//                        String tt = next.getName().replace("<clan>", c.getColorName());
//                        List<String> lore = new ArrayList<>(searchClanPlayer);
//                        lore.replaceAll(s3 -> s3.replace("<clan>", c.getColorName()));
//                        nextBnt.setLore(lore).setName(tt);
//                        e.setResult(nextBnt.build());
//                        ok = true;
//                        this.clan = c;
//                    } else {
//                        errorsBut.setLore(errSearchClanPlayer);
//                        e.setResult(errorsBut.build());
//                    }
//                } else
                if (na.startsWith("#")) {
                    try {
                        int id = Integer.valueOf(na.substring(1));
                        Iterator<Clan> ic = clans.stream().filter(clan1 -> clan1.getId() == id).limit(1).iterator();
                        if (ic.hasNext()) {
                            Clan c = ic.next();
                            String tt = next.getName().replace("<clan>", c.getColorName());
                            List<String> lore = new ArrayList<>(searchClanID);
                            lore.replaceAll(s -> s.replace("<clan>", c.getColorName()));
                            nextBnt.setLore(lore).setName(tt);
                            e.setResult(nextBnt.build());
                            ok = true;
                            this.clan = c;
                        } else {
                            errorsBut.setLore(errSearchClanID);
                            e.setResult(errorsBut.build());
                        }
                    } catch (Exception e2) {
                        errorsBut.setLore(errSearchIncorrectID);
                        e.setResult(errorsBut.build());
                    }
//                } else if (na.startsWith("@")) {
//                    String s = na.substring(1);
//                    Iterator<Clan> ic = clans.stream().filter(clan1 -> clan1.getColorTag().contentEquals(s)).limit(1).iterator();
//                    if (ic.hasNext()) {
//                        Clan c = ic.next();
//                        String tt = next.getName().replace("<clan>", c.getColorName());
//                        List<String> lore = new ArrayList<>(searchTag);
//                        lore.replaceAll(s3 -> s3.replace("<clan>", c.getColorName()).replace("<tag>", s));
//                        nextBnt.setLore(lore).setName(tt);
//                        e.setResult(nextBnt.build());
//                        ok = true;
//                        this.clan = c;
//                    } else {
//                        List<String> lore = new ArrayList<>(errSearchTag);
//                        lore.replaceAll(s1 -> s1.replace("<tag>", s));
//                        errorsBut.setLore(lore);
//                        e.setResult(errorsBut.build());
//                    }
                } else {
                    Iterator<Clan> ic = clans.stream().filter(clan1 -> clan1.getName().toLowerCase().contains(na.toLowerCase())).limit(1).iterator();
                    if (ic.hasNext()) {
                        Clan c = ic.next();
                        String tt = next.getName().replace("<clan>", c.getColorName());
                        List<String> lore = new ArrayList<>(searchClan);
                        lore.replaceAll(s3 -> s3.replace("<clan>", c.getColorName()));
                        nextBnt.setLore(lore).setName(tt);
                        e.setResult(nextBnt.build());
                        ok = true;
                        this.clan = c;
                    } else {
                        errorsBut.setLore(errSearchClan);
                        e.setResult(errorsBut.build());
                    }
                }


            }

            @Override
            public void onInventoryClick(InventoryClickEvent e) {
                final String name = this.name;
                switch (e.getRawSlot()) {
                    case 1:
                        MainMenu.open(p);
                        break;
                    case 2:
                        if (ok) {
                            runAcuns(() -> {
                                try {
                                    OtherClanMenu.open(p, null, clan, OtherClanMenu.OtherClanMenuType.info, OtherClanMenu.OtherMemberSort.top, 1);
                                } catch (Exception e1) {
                                    ErrorMenu.open(p);
                                    e1.printStackTrace();
                                }
                            });
                        }
                        break;
                    default:
                        break;
                }

            }

            @Override
            public void close() {
            }

            @Override
            public void open(AnvilInventory inv) {
                inv.setItem(0, infoBnt.build());
                inv.setItem(1, cancelBnt.build());
            }
        });
    }
}
