package ru.ilyshka_fox.clanfox.menus.anvil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import ru.ilyshka_fox.clanfox.core.menu.Item_builder;
import ru.ilyshka_fox.clanfox.core.menu.anvil.AnvilHolder;
import ru.ilyshka_fox.clanfox.core.menu.anvil.AnvilLisenger;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;
import ru.ilyshka_fox.clanfox.core.yamController.Contain;
import ru.ilyshka_fox.clanfox.core.yamController.Embedded;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;
import ru.ilyshka_fox.clanfox.data.Clan;
import ru.ilyshka_fox.clanfox.data.ClanPlayers;
import ru.ilyshka_fox.clanfox.menus.FColor;
import ru.ilyshka_fox.clanfox.menus.MenuEx;
import ru.ilyshka_fox.clanfox.menus.Question.QAdminInClan;
import ru.ilyshka_fox.clanfox.menus.defaylt.ErrorMenu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Contain(config = "Setting", path = "anvil.adminAddMembers")
public class adminAddMembers extends MenuEx {

    @Embedded
    public static itemHead info = new itemHead(" ", Collections.emptyList(), "NAME_TAG", "0", null, null);
    @Embedded
    public static itemHead cancel = new itemHead(FColor.BTN_BLACK + "Назад", Collections.emptyList(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", null, null);


    @Embedded
    private static itemHead next = new itemHead(ChatColor.RESET.toString(),
            Collections.singletonList(FColor.LORE + "Найден игрок " + FColor.INFO + "<player>"),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", null, null);
    @Embedded
    private static itemHead nexPlayerInClan = new itemHead(ChatColor.RESET.toString(),
            Arrays.asList(
                    FColor.LORE + "Найден игрок " + FColor.INFO + "<player>",
                    FColor.ERROR + "Он уже состоит в клане"),
            "SHIELD",
            "0", null, null);
    @Embedded
    private static itemHead errors = new itemHead(ChatColor.RESET.toString(),
            Collections.singletonList(FColor.ERROR + "Игрок не найден или его нету в сети"),
            "BARRIER",
            "0", null, null);


    public static void open(Player p, ClanPlayers cp, Clan c, Runnable open) {

        final Item_builder infoBnt = new Item_builder(info).localizedName("r");
        final Item_builder cancelBnt = new Item_builder(cancel).localizedName(ChatColor.RESET.toString());
        final Item_builder nextBnt = new Item_builder(next).localizedName(ChatColor.RESET.toString());
        final Item_builder nextBntPlayerInClan = new Item_builder(nexPlayerInClan).localizedName(ChatColor.RESET.toString());
        final Item_builder errorsBut = new Item_builder(errors).localizedName(ChatColor.RESET.toString());

        AnvilHolder.open(p, new AnvilLisenger() {
            String name = " ";
            boolean ok = false;
            ArrayList<Player> inClan = new ArrayList<>();
            ArrayList<Player> wite = new ArrayList<>();

            @Override
            public void onPrepareAnvil(PrepareAnvilEvent e) {
                String na = (e.getInventory().getRenameText().trim().equals("")) ? " " : e.getInventory().getRenameText().trim();
                if (na.equals(" ") || na.length() == 0)
                    return;
                Player p2 = Bukkit.getPlayer(na);
                ok = false;
                final String name = p2 == null ? na : p2.getName();

                if (p2 == null) {
                    // Если игрока нету на сервере то ошибка.
                    e.setResult(errorsBut.build());
                } else if (wite.contains(p2)) {
                    // если ужу проверялои то просто выводим правду.
                    List<String> lore = new ArrayList<>(next.getLore());
                    lore.replaceAll(t -> t.replaceFirst("<player>", name));
                    e.setResult(nextBnt.setLore(lore).name(next.getName().replaceFirst("<player>", name)).build());
                    this.name = name;
                    ok = true;
                } else if (inClan.contains(p2)) {
                    List<String> lore = new ArrayList<>(nexPlayerInClan.getLore());
                    lore.replaceAll(t -> t.replaceFirst("<player>", name));
                    e.setResult(nextBntPlayerInClan.setLore(lore).name(nexPlayerInClan.getName().replaceFirst("<player>", name)).build());
                    this.name = name;
                    ok = true;
                } else {
                    try {
                        ClanPlayers cpc = dbManager.getClanPlayers(name);
                        if (cpc.getClanid() > 0) {
                            inClan.add(p2);
                            List<String> lore = new ArrayList<>(nexPlayerInClan.getLore());
                            lore.replaceAll(t -> t.replaceFirst("<player>", name));
                            e.setResult(nextBntPlayerInClan.setLore(lore).name(nexPlayerInClan.getName().replaceFirst("<player>", name)).build());
                            this.name = name;
                            ok = true;
                        } else {
                            wite.add(p2);
                            List<String> lore = new ArrayList<>(next.getLore());
                            lore.replaceAll(t -> t.replaceFirst("<player>", name));
                            e.setResult(nextBnt.setLore(lore).name(next.getName().replaceFirst("<player>", name)).build());
                            this.name = name;
                            ok = true;
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        ErrorMenu.open(p);
                    }
                }
            }

            @Override
            public void onInventoryClick(InventoryClickEvent e) {
                final String name = this.name;
                switch (e.getRawSlot()) {
                    case 1:
                        open.run();
                        break;
                    case 2:
                        if (ok) {
                            runAcuns(() -> {
                                try {
                                    ClanPlayers cccc = dbManager.getClanPlayers(name);
                                    if (cccc.getClanid() > 0) {
                                        QAdminInClan.open(p, cccc, c, open);
                                    } else {
                                        dbManager.setPlayerClan(cccc.getNickName(), c.getId(), cccc.getId());
                                        open.run();
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                    ErrorMenu.open(p);
                                }
                            });
                        }
                        break;
                    default:
                        break;
                }

            }

            @Override
            public void close() {
            }

            @Override
            public void open(AnvilInventory inv) {
                inv.setItem(0, infoBnt.build());
                inv.setItem(1, cancelBnt.build());
            }
        });
    }
}
