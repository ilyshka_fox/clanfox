package ru.ilyshka_fox.clanfox.menus.defaylt;

import org.bukkit.entity.Player;
import ru.ilyshka_fox.clanfox.core.menu.GUIHolder;
import ru.ilyshka_fox.clanfox.core.yamController.Contain;
import ru.ilyshka_fox.clanfox.core.yamController.Value;
import ru.ilyshka_fox.clanfox.menus.FColor;


@Contain(config = "Setting", path = "menu.load", comment = "Показывается при ожидании загрузки")
public class LoadMenu {

    @Value
    public static String title = FColor.TITLE + "Загрузка...";
    @Value
    public static int line = 0;

    public static void open(Player p) {
        GUIHolder g = new GUIHolder(title, line);
        g.open(p, false);
    }

}
