package ru.ilyshka_fox.clanfox.menus.defaylt;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import ru.ilyshka_fox.clanfox.Main;
import ru.ilyshka_fox.clanfox.core.NMS.PlayerNMS;
import ru.ilyshka_fox.clanfox.core.menu.GUIHolder;
import ru.ilyshka_fox.clanfox.core.menu.Item_builder;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;
import ru.ilyshka_fox.clanfox.core.yamController.Contain;
import ru.ilyshka_fox.clanfox.core.yamController.Embedded;
import ru.ilyshka_fox.clanfox.core.yamController.Value;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;
import ru.ilyshka_fox.clanfox.core.yamController.global.constructor;
import ru.ilyshka_fox.clanfox.core.yamController.global.messages;
import ru.ilyshka_fox.clanfox.data.Clan;
import ru.ilyshka_fox.clanfox.data.ClanNews;
import ru.ilyshka_fox.clanfox.data.ClanPlayers;
import ru.ilyshka_fox.clanfox.data.ClanRank;
import ru.ilyshka_fox.clanfox.events.chat.reDescriptionClan;
import ru.ilyshka_fox.clanfox.events.chat.sendMail;
import ru.ilyshka_fox.clanfox.menus.FColor;
import ru.ilyshka_fox.clanfox.menus.MenuEx;
import ru.ilyshka_fox.clanfox.menus.Question.*;
import ru.ilyshka_fox.clanfox.menus.anvil.SearchPlayer;
import ru.ilyshka_fox.clanfox.menus.anvil.reNameClan;
import ru.ilyshka_fox.clanfox.menus.anvil.reNameTag;
import ru.ilyshka_fox.clanfox.menus.defaylt.ReIconMenu.IconSort;
import ru.ilyshka_fox.clanfox.setting.Config;
import ru.ilyshka_fox.clanfox.setting.Pex;

import java.util.*;

@Contain(path = "menu.myClan", config = "Setting")
public class MyClanMenu extends MenuEx {

    public enum MyClanMenuType {
        info, add, members, owner
    }

    @Value
    private static String title = FColor.TITLE + "Клан: <clanname>";

    //Основные кнопки
    @Embedded(path = "buttons.me.infoPlayer", comment = "Информация игрока и клана")
    private static itemHead infoPlayer;
    @Embedded(path = "buttons.me.statistics", comment = "Информация игрока")
    private static itemHead statistics;
    @Embedded(path = "buttons.me.playerRequest", comment = "<coint>")
    private static itemHead playerRequest;
    @Embedded(path = "buttons.me.members", comment = "<countmembers>")
    private static itemHead members;
    @Embedded(path = "buttons.me.owner")
    private static itemHead owner;
    @Embedded(path = "buttons.me.exit")
    private static itemHead exit;
    @Embedded(path = "buttons.me.cancel")
    private static itemHead cancel;
    // Кнопки инофрмации - почты
    @Embedded(path = "buttons.info.mail")
    private static itemHead mail;
    @Embedded(path = "buttons.info.mailTime", comment = {"<d> - день", "<h> - час", "<m> - минуты", "<s> - секунды"})
    private static itemHead mailTime;
    @Embedded(path = "buttons.info.news", comment = {"<title> - заголовок новости", "<msg> - сообщение", "<time:{format}> - время, {format} - формат времени по стандарту java"})
    private static itemHead news;
    // Кнопки приглашения
    @Embedded(path = "buttons.invitations.pageInvitations", comment = "<page>")
    private static itemHead pageInvite;
    @Embedded(path = "buttons.invitations.clanInvitations")
    private static itemHead clanInvite;
    @Embedded(path = "buttons.invitations.infoInvitations", comment = "Информация игрока")
    private static itemHead infoInvite;
    @Embedded(path = "buttons.invitations.acceptRequest", comment = "Информация игрока")
    private static itemHead acceptRequest;
    @Embedded(path = "buttons.invitations.denyRequest", comment = "Информация игрока")
    private static itemHead denyRequest;
    // Кнопки участников клана
    @Embedded(path = "buttons.invitations.pageMembers", comment = "<page>")
    private static itemHead pageMembers;
    @Embedded(path = "buttons.invitations.infoMembers")
    private static itemHead infoMembers;
    @Embedded(path = "buttons.invitations.upMembers", comment = {"Информация игрока", "<newRank> - предполагаемый ранг"})
    private static itemHead upMembers;
    @Embedded(path = "buttons.invitations.downMembers", comment = {"Информация игрока", "<newRank> - предполагаемый ранг"})
    private static itemHead downMembers;
    @Embedded(path = "buttons.invitations.kickMembers", comment = "Информация игрока")
    private static itemHead kickMembers;
    // Кнопки настрокий клана
    @Embedded(path = "buttons.setting.reName", comment = "Информация клана, <price>")
    private static itemHead reName;
    @Embedded(path = "buttons.setting.reTag", comment = "Информация клана, <price>")
    private static itemHead reTag;
    @Embedded(path = "buttons.setting.reIcon", comment = "Информация клана, <price>")
    private static itemHead reIcon;
    @Embedded(path = "buttons.setting.reDescriptions", comment = "Информация клана, <price>")
    private static itemHead reDescriptions;
    @Embedded(path = "buttons.setting.delClan", comment = "Информация клана, <price>")
    private static itemHead delClan;

    @Embedded(path = "buttons.setting.clanHome")
    private static itemHead clanHome = new itemHead(
            FColor.BTN_NAME + "Установить точку дома",
            Arrays.asList(
                    FColor.LORE + "Левая кнопка мыши - установить точку",
                    FColor.LORE + "Правая кнопка мыши - удалит точку"
            ),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTQ1MmFiYjk1MjhiYzNhZDQ4ZmYyYmI1MjU2YmY4YzBmZDE0NGZhYmZlNGM4ZTI4ZWJmMmVmMjllZTE1ZjkzNyJ9fX0=",
            13, null);
    @Value(path = "buttons.setting.clanHome.loreNoClanHome")
    private static List<String> noClanHome = Collections.singletonList(FColor.ERROR + "Точка клана не установленна");

    static {
        infoPlayer = new itemHead(
                FColor.BTN_NAME + "Информация",
                Arrays.asList(
                        FColor.LORE + "Вы: " + FColor.INFO + "<rank>",
                        "",
                        FColor.LORE + "   Клан",
                        FColor.LORE + "ID: " + FColor.INFO + "#<clanID> ",
                        FColor.LORE + "Рейтинг: " + FColor.INFO + "<point>",
                        FColor.LORE + "В топе: " + FColor.INFO + "<top> "
                ),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDI5NWE5MjkyMzZjMTc3OWVhYjhmNTcyNTdhODYwNzE0OThhNDg3MDE5Njk0MWY0YmZlMTk1MWU4YzZlZTIxYSJ9fX0=",
                2,
                null);
        statistics = new itemHead(FColor.BTN_NAME + "Статистика", Arrays.asList(
                FColor.LORE + "Убил: " + FColor.INFO + "<kills> ",
                FColor.LORE + "Погиб: " + FColor.INFO + "<death>",
                FColor.LORE + "Репутация: " + FColor.INFO + "<KD> ",
                FColor.LORE + "Статус в клане: " + FColor.INFO + "<rank>",
                FColor.INFO + "<online>"
        ), null,
                null,
                3,
                null);
        playerRequest = new itemHead(FColor.BTN_NAME + "Заявки в клан",
                Collections.singletonList(FColor.LORE + "Заявок: " + FColor.INFO + "<coint>"), "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2VkZDIwYmU5MzUyMDk0OWU2Y2U3ODlkYzRmNDNlZmFlYjI4YzcxN2VlNmJmY2JiZTAyNzgwMTQyZjcxNiJ9fX0=",
                4,
                null);
        members = new itemHead(FColor.BTN_NAME + "Учасники клана",
                Collections.singletonList(FColor.LORE + "Участников: " + FColor.INFO + "<countmembers> "), "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDkxM2E0YWYyZDMwNjQ3MTNmNzA0MTg3ZTU3ZTZiZWMzZGVlYzk0ZjFjMzM3MWIyYjM1YzZjNWU0MjdjYjAifX19",
                5,
                null);
        owner = new itemHead(FColor.BTN_NAME + "Панель управления", Collections.emptyList(), "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2JmYjQxZjg2NmU3ZThlNTkzNjU5OTg2YzlkNmU4OGNkMzc2NzdiM2Y3YmQ0NDI1M2U1ODcxZTY2ZDFkNDI0In19fQ==",
                6,
                null);
        exit = new itemHead(FColor.BTN_NAME + "Выход", Collections.emptyList(), "WOOD_DOOR",
                "0",
                null,
                null);
        cancel = new itemHead(FColor.BTN_BLACK + "Назад", Collections.emptyList(), "BARRIER",
                "0",
                8,
                null);

        mail = new itemHead(FColor.BTN_NAME + "Оповещения",
                Collections.singletonList(FColor.LORE + "Нажмине что-бы написать сообщение"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZWIyODE1Yjk5YzEzYmZjNTViNGM1YzI5NTlkMTU3YTYyMzNhYjA2MTg2NDU5MjMzYmMxZTRkNGY3ODc5MmM2OSJ9fX0=",
                null,
                null);
        mailTime = new itemHead(FColor.BTN_NAME + "Оповещения",
                Collections.singletonList(FColor.LORE + "Подождите еще " + FColor.INFO + "<d>д. <h>ч <m>м <s>с"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZWIyODE1Yjk5YzEzYmZjNTViNGM1YzI5NTlkMTU3YTYyMzNhYjA2MTg2NDU5MjMzYmMxZTRkNGY3ODc5MmM2OSJ9fX0=",
                null,
                null);
        news = new itemHead(FColor.BTN_NAME + "<title>",
                Arrays.asList(
                        FColor.INFO + "<msg>",
                        FColor.LORE + "Время: " + FColor.INFO + "<time:yyyy-MM-dd HH:mm:ss>"
                ),
                null, null, null, null);

        pageInvite = new itemHead(FColor.BTN_NAME + "Страница " + FColor.INFO + "<page>",
                Arrays.asList(
                        FColor.LORE + "Левая кнопка - вперед",
                        FColor.LORE + "Правая кнопка - назад"
                ),
                "PAPER",
                "0",
                null, null);
        clanInvite = new itemHead(FColor.BTN_NAME + "Пригласить игрока",
                Collections.emptyList(),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2VkZDIwYmU5MzUyMDk0OWU2Y2U3ODlkYzRmNDNlZmFlYjI4YzcxN2VlNmJmY2JiZTAyNzgwMTQyZjcxNiJ9fX0=",
                null, null);
        infoInvite = new itemHead(FColor.BTN_NAME + "<nickname>",
                Arrays.asList(
                        FColor.LORE + "Убил: " + FColor.INFO + "<kills>",
                        FColor.LORE + "Погиб: " + FColor.INFO + "<death>",
                        FColor.LORE + "Репутация: " + FColor.INFO + "<KD>",
                        FColor.LORE + "<online>"
                ),
                null, null, null, null);
        acceptRequest = new itemHead(FColor.BTN_NAME + "Принять заявк",
                Collections.singletonList(FColor.INFO + "<nickname>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==",
                null, null);
        denyRequest = new itemHead(FColor.BTN_NAME + "Отклонить заявку",
                Collections.singletonList(FColor.INFO + "<nickname>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==",
                null, null);

        pageMembers = new itemHead(FColor.BTN_NAME + "Страница " + FColor.INFO + "<page>",
                Arrays.asList(
                        FColor.LORE + "Левая кнопка - вперед",
                        FColor.LORE + "Правая кнопка - назад"
                ),
                "PAPER",
                "0",
                null, null);
        infoMembers = new itemHead(FColor.BTN_NAME + "<nickname>",
                Arrays.asList(
                        FColor.LORE + "Статус: " + FColor.INFO + "<rank>",
                        FColor.LORE + "Убил: " + FColor.INFO + "<kills>",
                        FColor.LORE + "Погиб: " + FColor.INFO + "<death>",
                        FColor.LORE + "Репутация: " + FColor.INFO + "<KD>",
                        FColor.INFO + "<online>"
                ),
                null, null, null, null);
        upMembers = new itemHead(FColor.BTN_NAME + "Повысить до " + FColor.INFO + "<newRank>",
                Arrays.asList(
                        FColor.LORE + "Игрок: " + FColor.INFO + "<nickname>",
                        FColor.LORE + "Статус: " + FColor.INFO + "<rank>"
                ),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzNjZjE2NmE4MjZkODU1NDU0ZWQ0ZDRlYTVmZTMzZjNkZWVhYTQ0Y2NhYTk5YTM0OGQzOTY4NWJhNzFlMWE0ZiJ9fX0=",
                null, null);
        downMembers = new itemHead(FColor.BTN_NAME + "Понизить до " + FColor.INFO + "<newRank>",
                Arrays.asList(
                        FColor.LORE + "Игрок: " + FColor.INFO + "<nickname>",
                        FColor.LORE + "Статус: " + FColor.INFO + "<rank>"
                ),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzhjYTc2MTVjYjgzN2Y2OTRlNDk2ZmY4YTk4NTNjZDdkYjVmZDg1NTI5ZGNhZDk4Yzc4YmEyNmMzZTRmNjg3In19fQ==",
                null, null);
        kickMembers = new itemHead(FColor.BTN_NAME + "Исключить из клана",
                Arrays.asList(
                        FColor.LORE + "Игрок: " + FColor.INFO + "<nickname>",
                        FColor.LORE + "Статус: " + FColor.INFO + "<rank>"
                ),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWE2Nzg3YmEzMjU2NGU3YzJmM2EwY2U2NDQ5OGVjYmIyM2I4OTg0NWU1YTY2YjVjZWM3NzM2ZjcyOWVkMzcifX19",
                null, null);
        kickMembers = new itemHead(FColor.BTN_NAME + "Исключить из клана",
                Arrays.asList(
                        FColor.LORE + "Игрок: " + FColor.INFO + "<nickname>",
                        FColor.LORE + "Статус: " + FColor.INFO + "<rank>"
                ),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWE2Nzg3YmEzMjU2NGU3YzJmM2EwY2U2NDQ5OGVjYmIyM2I4OTg0NWU1YTY2YjVjZWM3NzM2ZjcyOWVkMzcifX19",
                null, null);

        reName = new itemHead(FColor.BTN_NAME + "Переименовать клан",
                Arrays.asList(
                        FColor.LORE + "Имя клана: " + FColor.INFO + "<clanname>",
                        FColor.LORE + "Стоимость: " + FColor.INFO + "<price>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNmI0MGU1ZGIyMWNlZGFjNGM5NzJiN2IyMmViYjY0Y2Y0YWRkNjFiM2I1NGIxMzE0MzVlZWRkMzA3NTk4YjcifX19",
                20,
                null);
        reTag = new itemHead(FColor.BTN_NAME + "Изменить тег",
                Arrays.asList(
                        FColor.LORE + "Тег клана: " + FColor.INFO + "<tag>",
                        FColor.LORE + "Стоимость: " + FColor.INFO + "<price>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWRmODkwOTQ5OGMyNWY2ZTc1ZWYxOWUzNzZhN2Y4NGY2MWFmMjM0NTI1ZDYzOWJhNDYzZjk5MWY0YzgyZDAifX19",
                21,
                null);
        reIcon = new itemHead(FColor.BTN_NAME + "Изменить иконку",
                Collections.emptyList(),
                null,
                null,
                22,
                null);
        reDescriptions = new itemHead(FColor.BTN_NAME + "Изменить описание",
                Collections.singletonList(FColor.INFO + "<descriptions>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTU4NDQzMmFmNmYzODIxNjcxMjAyNThkMWVlZThjODdjNmU3NWQ5ZTQ3OWU3YjBkNGM3YjZhZDQ4Y2ZlZWYifX19",
                23,
                null);
        delClan = new itemHead(FColor.BTN_NAME + "Распустить клан",
                Collections.singletonList(FColor.ERROR + "Клан нельзя будет восстановить!!"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWQ2ZTc3ZjgyNDhhZTY0ZDdiODk0ZDY2YjY1NmE4MzVhMzMzM2RkNjk0YmY2MjRjODhlNjMxMGNmMzc2MzVkIn19fQ",
                24,
                null);

    }


    /**
     * Главное меню
     *
     * @param p {@link Player} Игрок которому нужно открыть меню.
     */
    public static void open(Player p, MyClanMenuType type, Object... data) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {
                ClanPlayers cp = dbManager.getClanPlayers(p.getName());
                if (cp.getClanid() <= 0) {
                    msgMenu.open(p, messages.noClan, () -> MainMenu.open(p));
                    return;
                }

                Clan c = dbManager.getClanThrows(cp.getClanid());

                GUIHolder g = new GUIHolder();
                Item_builder fonLine = new Item_builder(constructor.line);
                fonLine.name(ChatColor.RESET.toString()).setLore(new ArrayList<>());

                for (int i = 9; i < 18; i++) {
                    g.setButton(i, fonLine);
                }
                g.setTitle(c.replase(title));

                // Кнопка информации
                Item_builder bntInfo = new Item_builder(infoPlayer, (x) -> c.replase(cp.replase(x))).ItemListener((item, click) -> open(p, MyClanMenuType.info));
                g.setButton(infoPlayer.getSlot(), bntInfo);

                Item_builder bntStatistics = new Item_builder(statistics, (x) -> c.replase(cp.replase(x)));
                bntStatistics.material(Material.SKULL_ITEM);
                bntStatistics.configData(PlayerNMS.getSkinProfil(p));
                g.setButton(statistics.getSlot(), bntStatistics);

                int coint = dbManager.getCountRequest(c.getId());
                Item_builder newMembers = new Item_builder(playerRequest, (x) -> x.replaceFirst("<coint>", String.valueOf(coint))).ItemListener((item, click) -> open(p, MyClanMenuType.add, 1));
                g.setButton(playerRequest.getSlot(), newMembers);

                // участники
                Item_builder bntMembers = new Item_builder(members, c::replase).ItemListener((item, click) -> open(p, MyClanMenuType.members, 1));
                g.setButton(members.getSlot(), bntMembers);

                if (cp.getRank() == ClanRank.owner || cp.getRank() == ClanRank.coleader) {
                    Item_builder bntOwner = new Item_builder(owner).ItemListener((item, click) -> open(p, MyClanMenuType.owner));
                    g.setButton(owner.getSlot(), bntOwner);
                } else {
                    Item_builder bntExit = new Item_builder(exit).localizedName("EXIT").ItemListener((x, y) -> QExitClan.open(p, c));
                    g.setButton(owner.getSlot(), bntExit);
                }

                Item_builder bntCancel = new Item_builder(cancel).ItemListener((item, click) -> MainMenu.open(p));
                g.setButton(cancel.getSlot(), bntCancel);

                switch (type) {
                    // главна  - вывод почта клана.
                    case info: {
                        int pos = 18;
                        if (p.hasPermission(Pex.PEX_PLAYER_SMS) && cp.getRank().isPex(ClanRank.elder)) {
                            Item_builder bntMail;
                            // Узнаем время последнего сообщения
                            long time = System.currentTimeMillis() - (cp.getMsgTime() + Config.msgInterval * 60000);
                            if (time < 0 && !p.hasPermission(Pex.PEX_TIME_DELAY)) {
                                time = time * -1;
                                final long d = time / (1000 * 60 * 60 * 24);
                                time = time % (1000 * 60 * 60 * 24);
                                final long h = time / (1000 * 60 * 60);
                                time = time % (1000 * 60 * 60);
                                final long m = time / (1000 * 60);
                                time = time % (1000 * 60);
                                final long s = time / 1000;
                                String sD = String.valueOf(d);
                                String sH = String.format("%02d", h);
                                String sM = String.format("%02d", m);
                                String sS = String.format("%02d", s);
                                bntMail = new Item_builder(mailTime, (x) -> x.replaceFirst("<d>", sD)
                                        .replaceFirst("<h>", sH)
                                        .replaceFirst("<m>", sM)
                                        .replaceFirst("<s>", sS)
                                );
                            } else
                                bntMail = new Item_builder(mail).ItemListener((item, click) -> {
                                    p.closeInventory();
                                    p.sendMessage(messages.createMessage);
                                    sendMail.addReMail(p, c);
                                });
                            g.setButton(pos++, bntMail);
                        }
                        // Получаем список сообщений определенное количество
                        ArrayList<ClanNews> anews = dbManager.getClanNews(c.getId(), pos == 18 ? 36 : 35);
                        // Выводим сообщения
                        for (ClanNews i : anews) {
                            if (pos > 54)
                                break;
                            g.setButton(pos++, i.getItem(news));
                        }
                    }
                    break;
                    // Заявки в клан
                    case add: {
                        if (data.length < 1) {
                            throw new Exception("Data parameter - null");
                        }
                        for (int i = 16; i < 56; i = i + 9) {
                            g.setButton(i, fonLine);
                        }
                        int ipage = (int) data[0];
                        ArrayList<ClanPlayers> invate = dbManager.getInvitation(c.getId());
                        Item_builder page = new Item_builder(pageInvite, (x) -> x.replaceFirst("<page>", String.valueOf(data[0]))).amount(ipage).ItemListener((item, click) -> {
                            switch (click) {
                                case RIGHT:
                                    if (ipage > 1) {
                                        open(p, MyClanMenuType.add, ipage - 1);
                                    }
                                    break;
                                case LEFT:
                                    int max = invate.size() / 28;
                                    if (c.getCollMembers() % 28 > 0)
                                        max++;
                                    if (ipage < max) {
                                        open(p, MyClanMenuType.add, ipage + 1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        });
                        g.setButton(26, page);
                        if (p.hasPermission(Pex.PEX_PLAYER_INVITE) && cp.getRank().toInt() >= ClanRank.elder.toInt()) {
                            Item_builder addNew = new Item_builder(clanInvite).ItemListener((item, click) -> {
                                if (c.getCollMembers() >= Config.clanSize && Config.clanSize > 0) {
                                    msgMenu.open(p, messages.clanFull, () -> open(p, type, data));
                                    return;
                                }
                                if (p.hasPermission(Pex.PEX_PLAYER_INVITE))
                                    SearchPlayer.open(p, cp, c);
                                else
                                    msgMenu.open(p, messages.noPex, () -> open(p, type, data));
                            });

                            g.setButton(35, addNew);
                        }
                        // проверка вывода..
                        if (invate.size() > (ipage - 1) * 24) {
                            int i = (ipage - 1) * 28;
                            for (int ii = 0; ii < 4; ii++) {
                                for (int jj = 0; jj < 7; jj++) {
                                    if (i < invate.size()) {
                                        ClanPlayers pl = invate.get(i++);
                                        g.setButton((2 + ii) * 9 + jj, pl.getButton(infoInvite).configData(pl.getSkin()).ItemListener((item, click) -> {
                                            if (p.hasPermission(Pex.PEX_CLAN_INVATE_ACCEPT)) {
                                                g.setButton(35, null);
                                                g.setButton(44, null);
                                                g.setButton(53, null);
                                                if (cp.getId() != pl.getId())
                                                    if (cp.getRank().toInt() > 2) {
                                                        ClanRank r = pl.getRank();
                                                        if (cp.getRank().isPex(r)) {
                                                            g.setButton(35, new Item_builder(acceptRequest, pl::replase).ItemListener((item2, click2) -> {
                                                                if (c.getCollMembers() >= Config.clanSize && Config.clanSize > 0) {
                                                                    msgMenu.open(p, messages.clanFull, () -> open(p, type, data));
                                                                    return;
                                                                }
                                                                QAcceptRequest.open(p, cp, pl, () -> open(p, MyClanMenuType.add, ipage, pl.getNickName()));
                                                            }));
                                                            g.setButton(44, new Item_builder(denyRequest, pl::replase).ItemListener((item2, click2) ->
                                                                    QDenyRequest.open(p, cp, pl, () -> open(p, MyClanMenuType.add, ipage, pl.getNickName()))));
                                                        }
                                                    }
                                                g.update();
                                            } else
                                                msgMenu.open(p, messages.noPex, () -> open(p, type, data));
                                        }));
                                        if (data.length > 1) {
                                            if (cp.getRank().toInt() > 2 && pl.getNickName().equalsIgnoreCase((String) data[1])) {
                                                ClanRank r = pl.getRank();
                                                if (cp.getRank() == ClanRank.owner || pl.getRank().isPex(r)) {
                                                    g.setButton(35, new Item_builder(acceptRequest, pl::replase).ItemListener((item2, click2) -> QAcceptRequest.open(p, cp, pl, () -> open(p, MyClanMenuType.add, ipage, pl.getNickName()))));
                                                    g.setButton(44, new Item_builder(denyRequest, pl::replase).ItemListener((item2, click2) -> QDenyRequest.open(p, cp, pl, () -> open(p, MyClanMenuType.add, ipage, pl.getNickName()))));
                                                }
                                            }
                                        }

                                    } else
                                        break;

                                }
                                if (i >= invate.size())
                                    break;
                            }

                        }

                    }
                    break;
                    // Участники клана
                    case members: {
                        // Участники клана
                        if (data.length < 1) {
                            throw new Exception("Data parametr - null");
                        }
                        for (int i = 16; i < 56; i = i + 9) {
                            g.setButton(i, fonLine);
                        }
                        int ipage = (int) data[0];
                        Item_builder page = new Item_builder(pageMembers, (x) -> x.replaceFirst("<page>", String.valueOf(data[0]))).amount(ipage).ItemListener((item, click) -> {
                            switch (click) {
                                case RIGHT:
                                    if (ipage > 1) {
                                        open(p, MyClanMenuType.members, ipage - 1);
                                    }
                                    break;
                                case LEFT:
                                    int max = c.getCollMembers() / 28;
                                    if (c.getCollMembers() % 28 > 0)
                                        max++;
                                    if (ipage < max) {
                                        open(p, MyClanMenuType.members, ipage + 1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        });
                        g.setButton(26, page);

                        if (c.getCollMembers() > (ipage - 1) * 24) {
                            int i = (ipage - 1) * 28;
                            for (int ii = 0; ii < 4; ii++) {
                                for (int jj = 0; jj < 7; jj++) {
                                    if (i < c.getMembers().size()) {
                                        ClanPlayers pl = c.getMembers().get(i++);
                                        g.setButton((2 + ii) * 9 + jj, pl.getButton(infoMembers).amount(pl.getRank().toInt()).configData(pl.getSkin()).ItemListener((item, click) -> {
                                            if (!p.hasPermission(Pex.PEX_CLAN_EDIT_MEMBERS) && !p.hasPermission(Pex.PEX_ADMIN_MEMBERS)) {
                                                msgMenu.open(p, messages.noPex, () -> open(p, type, data));
                                                return;
                                            }
                                            g.setButton(35, null);
                                            g.setButton(44, null);
                                            g.setButton(53, null);
                                            if (cp.getId() != pl.getId())
                                                if (cp.getRank().toInt() > 2) {
                                                    ClanRank r = pl.getRank();
                                                    if (cp.getRank() == ClanRank.owner || cp.getRank().isPex(r)) {
                                                        if (r.toInt() < 4)
                                                            g.setButton(35, new Item_builder(upMembers, (s) -> pl.replase(s.replaceFirst("<newRank>", ClanRank.valueOf(pl.getRank().toInt() + 1).toString()))).ItemListener((item2, click2) -> QPlayerUp.open(p, cp, pl, false, () -> open(p, MyClanMenuType.members, ipage, pl.getNickName()))));

                                                        if (r.toInt() > 1)
                                                            g.setButton(44, new Item_builder(downMembers, (s) -> pl.replase(s.replaceFirst("<newRank>", ClanRank.valueOf(pl.getRank().toInt() - 1).toString()))).ItemListener((item2, click2) -> QPlayerDown.open(p, pl, () -> open(p, MyClanMenuType.members, ipage, pl.getNickName()))));
                                                        g.setButton(53, new Item_builder(kickMembers, pl::replase).ItemListener((item2, click2) -> QPlayerKick.open(p, pl, () -> open(p, MyClanMenuType.members, ipage))));
                                                    }
                                                }
                                            g.update();
                                        }));

                                        if (data.length > 1) {
                                            if (cp.getRank().toInt() > 2 && pl.getNickName().equalsIgnoreCase((String) data[1])) {
                                                ClanRank r = pl.getRank();
                                                if (cp.getRank() == ClanRank.owner || pl.getRank().isPex(r)) {
                                                    if (r.toInt() < 4)
                                                        g.setButton(35, new Item_builder(upMembers, (s) -> pl.replase(s.replaceFirst("<newRank>", ClanRank.valueOf(pl.getRank().toInt() + 1).toString()))).ItemListener((item2, click2) -> QPlayerUp.open(p, cp, pl, false, () -> open(p, MyClanMenuType.members, ipage, pl.getNickName()))));

                                                    if (r.toInt() > 1)
                                                        g.setButton(44, new Item_builder(downMembers, (s) -> pl.replase(s.replaceFirst("<newRank>", ClanRank.valueOf(pl.getRank().toInt() - 1).toString()))).ItemListener((item2, click2) -> QPlayerDown.open(p, pl, () -> open(p, MyClanMenuType.members, ipage, pl.getNickName()))));
                                                    g.setButton(53, new Item_builder(kickMembers, pl::replase).ItemListener((item2, click2) -> QPlayerKick.open(p, pl, () -> open(p, MyClanMenuType.members, ipage))));
                                                }
                                            }
                                        }

                                    } else
                                        break;

                                }
                                if (i >= c.getCollMembers())
                                    break;
                            }

                        }

                    }
                    break;
                    // Подменю управления кланом.
                    case owner: {
                        Item_builder fon = new Item_builder(constructor.fonSetting).name(ChatColor.RESET.toString()).localizedName("fon");
                        for (int i = 18; i < 56; i++)
                            g.setButton(i, fon);
                        for (int i = 9; i < 18; i++)
                            g.setButton(i, fonLine);


                        Item_builder bntClanHome = new Item_builder(clanHome).localizedName("clanHome");

                        if (p.hasPermission(Pex.PEX_ADMIN_CLAN) || p.hasPermission(Pex.PEX_PLAYER_CLANHOME)) {
                            bntClanHome.ItemListener((ItemStack item, ClickType click) -> {
                                LoadMenu.open(p);
                                switch (click) {
                                    case RIGHT:
                                        runAcuns(() -> {
                                            try {
                                                dbManager.setLocation(c.getId(), "");
                                                msgMenu.open(p, messages.clearClanHome, () -> open(p, type, data));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                ErrorMenu.open(p);
                                            }
                                        });
                                        break;
                                    default:
                                        if (!Config.vault || p.hasPermission(Pex.PEX_ECO_SETHOME) || p.hasPermission(Pex.PEX_ADMIN_CLAN) || Main.econ.has(p, Config.setHomePrice)) {
                                            Location l = p.getLocation();
                                            StringBuilder loc = new StringBuilder();
                                            loc.append(l.getWorld().getName());
                                            loc.append(":");
                                            loc.append(l.getX());
                                            loc.append(":");
                                            loc.append(l.getY());
                                            loc.append(":");
                                            loc.append(l.getZ());
                                            loc.append(":");
                                            loc.append(l.getPitch());
                                            loc.append(":");
                                            loc.append(l.getYaw());
                                            if (Config.debug)
                                                Main.sendMSGColor(ChatColor.AQUA + "Location: " + loc.toString());

                                            runAcuns(() -> {
                                                try {
                                                    dbManager.setLocation(c.getId(), loc.toString());
                                                    msgMenu.open(p, messages.setClanHome, () -> open(p, type, data));

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    ErrorMenu.open(p);
                                                }
                                            });
                                        } else
                                            msgMenu.open(p, messages.noMoney, () -> open(p, type, data));
                                        break;
                                }
                            });
                        } else {
                            bntClanHome.ItemListener((ItemStack item, ClickType click) -> {
                                msgMenu.open(p, messages.noPex, () -> open(p, type, data));
                            });
                        }
                        if (c.getClanHome() == null) {
                            bntClanHome.setLore(noClanHome);
                        }
                        bntClanHome.replaseAll(s->s.replace("<price>",String.valueOf(Config.setHomePrice)));
                        g.setButton(clanHome.getSlot() + 18, bntClanHome);

                        String priceRename = Config.renamePrice == 0 || p.hasPermission(Pex.PEX_ECO_RENAME) || p.hasPermission(Pex.PEX_ADMIN_CLAN) ? constructor.free : String.valueOf(Config.renamePrice);
                        Item_builder bntReName = new Item_builder(reName, s -> c.replase(s.replace("<price>", priceRename))).localizedName("reName").ItemListener((item, click) ->
                        {
                            if (!p.hasPermission(Pex.PEX_PLAYER_RENAME) && !p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
                                msgMenu.open(p, messages.noPex, () -> open(p, type, data));
                            } else if (!Config.vault || p.hasPermission(Pex.PEX_ADMIN_CLAN) || p.hasPermission(Pex.PEX_ECO_RENAME) || Main.econ.has(p, Config.renamePrice))
                                reNameClan.open(p, c, () -> open(p, type, data));
                            else
                                msgMenu.open(p, messages.noMoney, () -> open(p, type, data));
                        });
                        g.setButton(reName.getSlot() + 18, bntReName);

                        if (!Config.nameToTag) {
                            String priceReTag = Config.reTagPrice == 0 || p.hasPermission(Pex.PEX_ECO_RETAG) || p.hasPermission(Pex.PEX_ADMIN_CLAN) ? constructor.free : String.valueOf(Config.reTagPrice);
                            Item_builder bntReTag = new Item_builder(reTag, s -> c.replase(s.replace("<price>", priceReTag))).localizedName("reTag").ItemListener((item, click) -> {
                                if (!p.hasPermission(Pex.PEX_PLAYER_RETEG) && !p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
                                    msgMenu.open(p, messages.noPex, () -> open(p, type, data));
                                } else if (!Config.vault || p.hasPermission(Pex.PEX_ECO_RETAG) || p.hasPermission(Pex.PEX_ADMIN_CLAN) || Main.econ.has(p, Config.reTagPrice))
                                    reNameTag.open(p, c, () -> open(p, type, data));
                                else
                                    msgMenu.open(p, messages.noMoney, () -> open(p, type, data));
                            });
                            g.setButton(reTag.getSlot() + 18, bntReTag);
                        }
                        Item_builder bntReIcon = new Item_builder(reIcon, c::replase).material(Material.SKULL_ITEM).configData(c.getHead()).localizedName("reIcon").ItemListener((item, click) -> {
                            if (!p.hasPermission(Pex.PEX_PLAYER_ICO) && !p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
                                msgMenu.open(p, messages.noPex, () -> open(p, type, data));
                            } else
                                ReIconMenu.open(p, c, 1, IconSort.name, () -> open(p, type, data));
                        });
                        g.setButton(reIcon.getSlot() + 18, bntReIcon);

                        Item_builder bntReDescriptions = new Item_builder(reDescriptions, c::replase).localizedName("reDescriptions").ItemListener((item, click) -> {
                            if (!p.hasPermission(Pex.PEX_PLAYER_REOPIS) && !p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
                                msgMenu.open(p, messages.noPex, () -> open(p, type, data));
                            } else {
                                p.sendMessage(messages.reDescriptions);
                                reDescriptionClan.addReDiscription(p, c, () -> open(p, type, data));
                            }
                        });
                        ArrayList<String> lore = new ArrayList<>();
                        for (String s : Objects.requireNonNull(bntReDescriptions.getLore())) {
                            if (s.contains("<descriptions>"))
                                lore.addAll(c.getArrayOpis(s));
                            else
                                lore.add(s);
                        }
                        bntReDescriptions.setLore(lore);
                        g.setButton(reDescriptions.getSlot() + 18, bntReDescriptions);
                        if (cp.getRank() == ClanRank.owner) {
                            Item_builder bntDelClan = new Item_builder(delClan, c::replase).localizedName("delClan")
                                    .ItemListener((item, click) -> {
                                        if (p.hasPermission(Pex.PEX_PLAYER_DELETE))
                                            QDisbandClan.open(p, c, () -> open(p, type, data));
                                        else
                                            msgMenu.open(p, messages.noPex, () -> open(p, type, data));

                                    });
                            g.setButton(delClan.getSlot() + 18, bntDelClan);
                        } else {
                            // выход из клана
                            Item_builder bntExit = new Item_builder(exit, c::replase).localizedName("EXIT").ItemListener((x, y) -> {
                                if (p.hasPermission(Pex.PEX_PLAYER_EXIT))
                                    QExitClan.open(p, c);
                                else
                                    msgMenu.open(p, messages.noPex, () -> open(p, type, data));
                            });
                            g.setButton(delClan.getSlot() + 18, bntExit);
                        }
                        break;
                    }
                    default:
                        break;
                }

                g.open(p);
            } catch (Exception e) {
                e.printStackTrace();
                Bukkit.getScheduler().runTask(Main.plugin, () -> ErrorMenu.open(p));
            }
        });
    }

}