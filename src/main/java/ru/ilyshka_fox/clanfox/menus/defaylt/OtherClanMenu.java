package ru.ilyshka_fox.clanfox.menus.defaylt;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import ru.ilyshka_fox.clanfox.Main;
import ru.ilyshka_fox.clanfox.core.menu.GUIHolder;
import ru.ilyshka_fox.clanfox.core.menu.Item_builder;
import ru.ilyshka_fox.clanfox.core.sql.dbManager;
import ru.ilyshka_fox.clanfox.core.yamController.Contain;
import ru.ilyshka_fox.clanfox.core.yamController.Embedded;
import ru.ilyshka_fox.clanfox.core.yamController.Value;
import ru.ilyshka_fox.clanfox.core.yamController.contructor.itemHead;
import ru.ilyshka_fox.clanfox.core.yamController.global.constructor;
import ru.ilyshka_fox.clanfox.core.yamController.global.messages;
import ru.ilyshka_fox.clanfox.data.Clan;
import ru.ilyshka_fox.clanfox.data.ClanPlayers;
import ru.ilyshka_fox.clanfox.data.ClanRank;
import ru.ilyshka_fox.clanfox.events.chat.reDescriptionClan;
import ru.ilyshka_fox.clanfox.menus.FColor;
import ru.ilyshka_fox.clanfox.menus.MenuEx;
import ru.ilyshka_fox.clanfox.menus.Question.QDisbandClan;
import ru.ilyshka_fox.clanfox.menus.Question.QPlayerDown;
import ru.ilyshka_fox.clanfox.menus.Question.QPlayerKick;
import ru.ilyshka_fox.clanfox.menus.Question.QPlayerUp;
import ru.ilyshka_fox.clanfox.menus.anvil.adminAddMembers;
import ru.ilyshka_fox.clanfox.menus.anvil.reNameClan;
import ru.ilyshka_fox.clanfox.menus.anvil.reNameTag;
import ru.ilyshka_fox.clanfox.setting.Config;
import ru.ilyshka_fox.clanfox.setting.Pex;

import java.util.*;

@Contain(path = "menu.otherClan", config = "Setting")
public class OtherClanMenu extends MenuEx {

    public enum OtherClanMenuType {
        info, admin
    }

    public enum OtherMemberSort {
        top, name, lvl
    }

    @Value
    private static String title = FColor.TITLE + "Клан: " + FColor.INFO + "<clanname>";

    @Embedded(path = "buttons.info", comment = "Информация клана")
    private static itemHead info = new itemHead(
            FColor.BTN_NAME + "Информация",
            Arrays.asList(
                    FColor.LORE + "Клан: " + FColor.INFO + "#<clanID> " + FColor.LORE + "| " + FColor.INFO + "<clanname>",
                    FColor.LORE + "Рейтинг " + FColor.INFO + "<point>",
                    FColor.LORE + "Участников: " + FColor.INFO + "<countmembers>",
                    FColor.LORE + "Описание:",
                    FColor.INFO + "<descriptions>"
            ),
            null,
            null,
            0,
            null);

    @Embedded(path = "buttons.owner")
    private static itemHead owner = new itemHead(
            FColor.BTN_NAME + "Редактировать клан",
            Collections.emptyList(),
            "COMMAND",
            "0",
            7,
            null);

    @Embedded(path = "buttons.cancel")
    private static itemHead cancel = new itemHead(
            FColor.BTN_BLACK + "Назад",
            Collections.emptyList(),
            "BARRIER",
            "0",
            8,
            null);
    @Embedded(path = "buttons.invitations.pageMembers", comment = "<page>")
    private static itemHead pageMembers = new itemHead(
            FColor.BTN_NAME + "Страница " + FColor.INFO + "<page>",
            Arrays.asList(
                    FColor.LORE + "Левая кнопка - вперед",
                    FColor.LORE + "Правая кнопка - назад"
            ),
            "PAPER",
            "0",
            null, null);
    @Embedded(path = "buttons.members", comment = "Информация игрока")
    private static itemHead members = new itemHead(FColor.BTN_NAME + "<nickname>", Arrays.asList(
            FColor.LORE + "Убил " + FColor.INFO + "<kills>",
            FColor.LORE + "Погиб: " + FColor.INFO + "<death>",
            FColor.LORE + "Репутация: " + FColor.INFO + "<KD>",
            FColor.LORE + "<online>"
    ), null,
            null,
            null,
            null);


    @Embedded(path = "buttons.inClan")
    private static itemHead inClan = new itemHead(FColor.BTN_NAME + "Вы уже состоите в клане",
            Collections.emptyList(),
            "SHIELD",
            "0",
            1,
            null);

    @Embedded(path = "buttons.playerRequest")
    private static itemHead playerRequest = new itemHead(FColor.BTN_NAME + "Вы уже подали заявку в клан",
            Collections.emptyList(),
            "ARROW",
            "0",
            1,
            null);

    @Embedded(path = "buttons.clanInvitation")
    private static itemHead clanInvitation = new itemHead(FColor.BTN_NAME + "Приянть приглашение от клана",
            Collections.emptyList(),
            "DIAMOND_SWORD",
            "0",
            1,
            null);

    @Embedded(path = "buttons.clanBan")
    private static itemHead clanBan = new itemHead(FColor.BTN_NAME + "Вы в черном списке клана",
            Collections.emptyList(),
            "IRON_FENCE",
            "0",
            1,
            null);
    @Embedded(path = "buttons.apply")
    private static itemHead apply = new itemHead(FColor.BTN_NAME + "Подать заявку в вклан",
            Collections.emptyList(),
            "SIGN",
            "0",
            1,
            null);

    @Embedded(path = "buttons.admin.upMembers", comment = {"Информация игрока", "<newRank> - предполагаемый ранг"})
    private static itemHead upMembers = new itemHead(FColor.BTN_NAME + "Повысить до " + FColor.INFO + "<newRank>",
            Arrays.asList(
                    FColor.LORE + "Игрок: " + FColor.INFO + "<nickname>",
                    FColor.LORE + "Статус: " + FColor.INFO + "<rank>"
            ),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzNjZjE2NmE4MjZkODU1NDU0ZWQ0ZDRlYTVmZTMzZjNkZWVhYTQ0Y2NhYTk5YTM0OGQzOTY4NWJhNzFlMWE0ZiJ9fX0=",
            null, null);

    @Embedded(path = "buttons.admin.downMembers", comment = {"Информация игрока", "<newRank> - предполагаемый ранг"})
    private static itemHead downMembers = new itemHead(FColor.BTN_NAME + "Понизить до " + FColor.INFO + "<newRank>",
            Arrays.asList(
                    FColor.LORE + "Игрок: " + FColor.INFO + "<nickname>",
                    FColor.LORE + "Статус: " + FColor.INFO + "<rank>"
            ),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzhjYTc2MTVjYjgzN2Y2OTRlNDk2ZmY4YTk4NTNjZDdkYjVmZDg1NTI5ZGNhZDk4Yzc4YmEyNmMzZTRmNjg3In19fQ==",
            null, null);

    @Embedded(path = "buttons.admin.kickMembers", comment = "Информация игрока")
    private static itemHead kickMembers = new itemHead(FColor.BTN_NAME + "Исключить из клана",
            Arrays.asList(
                    FColor.LORE + "Игрок: " + FColor.INFO + "<nickname>",
                    FColor.LORE + "Статус: " + FColor.INFO + "<rank>"
            ),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWE2Nzg3YmEzMjU2NGU3YzJmM2EwY2U2NDQ5OGVjYmIyM2I4OTg0NWU1YTY2YjVjZWM3NzM2ZjcyOWVkMzcifX19",
            null, null);

    // Кнопки настрокий клана
    @Embedded(path = "buttons.admin.reName", comment = "Информация клана")
    private static itemHead reName;
    @Embedded(path = "buttons.admin.reTag", comment = "Информация клана")
    private static itemHead reTag;
    @Embedded(path = "buttons.admin.reIcon", comment = "Информация клана")
    private static itemHead reIcon;
    @Embedded(path = "buttons.admin.reDescriptions", comment = "Информация клана")
    private static itemHead reDescriptions;
    @Embedded(path = "buttons.admin.delClan", comment = "Информация клана")
    private static itemHead delClan;
    @Embedded(path = "buttons.admin.addMembers", comment = "Информация клана")
    private static itemHead addMembers;

    @Embedded(path = "buttons.admin.clanHome")
    private static itemHead clanHome = new itemHead(
            FColor.BTN_NAME + "Установить точку дома",
            Arrays.asList(
                    FColor.LORE + "Левая кнопка мыши - установить точку",
                    FColor.LORE + "Правая кнопка мыши - удалит точку"
            ),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTQ1MmFiYjk1MjhiYzNhZDQ4ZmYyYmI1MjU2YmY4YzBmZDE0NGZhYmZlNGM4ZTI4ZWJmMmVmMjllZTE1ZjkzNyJ9fX0=",
            14, null);
    @Value(path = "buttons.admin.clanHome.loreNoClanHome")
    private static List<String> noClanHome = Collections.singletonList(FColor.ERROR + "Точка клана не установленна");


    static {
        reName = new itemHead(FColor.BTN_NAME + "Переименовать клан",
                Collections.singletonList(FColor.LORE + "Имя клана: " + FColor.INFO + "<clanname>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNmI0MGU1ZGIyMWNlZGFjNGM5NzJiN2IyMmViYjY0Y2Y0YWRkNjFiM2I1NGIxMzE0MzVlZWRkMzA3NTk4YjcifX19",
                20,
                null);
        reTag = new itemHead(FColor.BTN_NAME + "Изменить тег",
                Collections.singletonList(FColor.LORE + "Тег клана: " + FColor.INFO + "<tag>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWRmODkwOTQ5OGMyNWY2ZTc1ZWYxOWUzNzZhN2Y4NGY2MWFmMjM0NTI1ZDYzOWJhNDYzZjk5MWY0YzgyZDAifX19",
                21,
                null);
        reIcon = new itemHead(FColor.BTN_NAME + "Изменить иконку",
                Collections.emptyList(),
                null,
                null,
                22,
                null);
        reDescriptions = new itemHead(FColor.BTN_NAME + "Изменить описание",
                Collections.singletonList(FColor.INFO + "<descriptions>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTU4NDQzMmFmNmYzODIxNjcxMjAyNThkMWVlZThjODdjNmU3NWQ5ZTQ3OWU3YjBkNGM3YjZhZDQ4Y2ZlZWYifX19",
                23,
                null);
        delClan = new itemHead(FColor.BTN_NAME + "Распустить клан",
                Collections.singletonList(FColor.ERROR + "Клан нельзя будет восстановить!!"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWQ2ZTc3ZjgyNDhhZTY0ZDdiODk0ZDY2YjY1NmE4MzVhMzMzM2RkNjk0YmY2MjRjODhlNjMxMGNmMzc2MzVkIn19fQ",
                24,
                null);
        addMembers = new itemHead(FColor.BTN_NAME + "Добавить участника",
                Collections.emptyList(),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZGY2NGJjOTI3ZDVhN2NjNGNlMzM0NTU5OTNmM2Y5MzY3YTBjYmZlZTU4MjJkNTUyOWYyZDg0NzU0MTFmMyJ9fX0=",
                12,
                null);
    }

    public static void open(Player p, ClanPlayers cp, int openClanID, OtherClanMenuType type, OtherMemberSort sortMembers, int page) {
        if (!p.hasPermission(Pex.PEX_PLAYER_OTHERCLAN) && !p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
            msgMenu.open(p, messages.noPex, () -> MainMenu.open(p));
            return;
        }
        try {
            Clan c = dbManager.getClan(openClanID);
            open(p, cp, c, type, sortMembers, page);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorMenu.open(p);
        }

    }

    /**
     * Меню другово клана
     *
     * @param p        игрок которому открыть
     * @param cp       Экземпляр инофрмации игрока можно null
     * @param openClan клан который проматриваем
     * @param type     тип открываемого меню
     * @param page     страницу игроков
     */
    public static void open(Player p, ClanPlayers cp, Clan openClan, OtherClanMenuType type, OtherMemberSort sortMembers, int page) {
        if (!p.hasPermission(Pex.PEX_PLAYER_OTHERCLAN) && !p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
            msgMenu.open(p, messages.noPex, () -> MainMenu.open(p));
            return;
        }
        LoadMenu.open(p);
        runAcuns(() -> {
            try {
                openClan.updateMembers();
                final ClanPlayers clanPlayer = cp == null ? dbManager.getClanPlayers(p.getName()) : cp;
                GUIHolder g = new GUIHolder();
                Item_builder fonLine = new Item_builder(constructor.line).name(ChatColor.RESET.toString()).setLore(new ArrayList<>());
                for (int i = 9; i < 18; i++) {
                    g.setButton(i, fonLine);
                }
                g.setTitle(openClan.replase(title));

                // Кнопка информации
                Item_builder bntInfo = openClan.getButton(info).ItemListener((item, click) -> open(p, clanPlayer, openClan, OtherClanMenuType.info, sortMembers, page));
                g.setButton(info.getSlot(), bntInfo);

                // Кнопка назад
                Item_builder bntCancel = new Item_builder(cancel).ItemListener((item, click) -> MainMenu.open(p));
                g.setButton(cancel.getSlot(), bntCancel);

                if (p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
                    Item_builder bntOwner = new Item_builder(owner, (x) -> openClan.replase(clanPlayer.replase(x))).ItemListener((item, click) -> open(p, clanPlayer, openClan, OtherClanMenuType.admin, sortMembers, page));
                    g.setButton(owner.getSlot(), bntOwner);
                }

                if (clanPlayer.getClanid() > 0) {
                    // Вы уже состоитев клане
                    g.setButton(inClan.getSlot(), new Item_builder(inClan));
                } else if (dbManager.isClanRequest(openClan.getId(), clanPlayer.getId())) {
                    // вы уже подали заявку в клан
                    g.setButton(playerRequest.getSlot(), new Item_builder(playerRequest));
                } else if (dbManager.isClanInvate(openClan.getId(), clanPlayer.getId())) {
                    // Клан пригласил вас
                    g.setButton(clanInvitation.getSlot(), new Item_builder(clanInvitation).ItemListener((i, x) -> {
                        try {
                            if (dbManager.isPlayerClan(clanPlayer.getId())) {
                                dbManager.clearPlayerInvitation(clanPlayer.getId());
                                dbManager.clearPlayerRequest(clanPlayer.getId());
                                msgMenu.open(p, messages.inClan, () -> open(p, null, openClan, type, sortMembers, page));
                            } else {
                                dbManager.setPlayerClan(clanPlayer.getNickName(), openClan.getId(), clanPlayer.getId());
                                open(p, null, openClan, type, sortMembers, page);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            ErrorMenu.open(p);
                        }
                    }));
                } else if (dbManager.isBanNameInClan(openClan.getId(), clanPlayer.getId())) {
                    // Вы в черном списке клана
                    g.setButton(clanBan.getSlot(), new Item_builder(clanBan));
                } else {
                    g.setButton(playerRequest.getSlot(), new Item_builder(apply).ItemListener((item, action) -> {
                        if (openClan.getCollMembers() >= Config.clanSize && Config.clanSize > 0 ) {
                            msgMenu.open(p, messages.clanFull, () -> open(p, cp, openClan, type, sortMembers, page));
                            return;
                        }
                        if (p.hasPermission(Pex.PEX_CLAN_INVITE)) {
                            try {
                                if (dbManager.isPlayerClan(clanPlayer.getId())) {
                                    msgMenu.open(p, messages.inClan, () -> open(p, null, openClan, type, sortMembers, page));
                                } else {
                                    dbManager.newRequest(openClan.getId(), clanPlayer.getId());
                                    open(p, clanPlayer, openClan, type, sortMembers, page);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                ErrorMenu.open(p);
                            }
                        } else
                            msgMenu.open(p, messages.noPex, () -> MainMenu.open(p));

                    }));
                }


                switch (type) {
                    case info: {
                        for (int i = 16; i < 56; i = i + 9) {
                            g.setButton(i, fonLine);
                        }

                        Item_builder iPage = new Item_builder(pageMembers, (x) -> x.replaceFirst("<page>", String.valueOf(page))).amount(page).ItemListener((item, click) -> {
                            switch (click) {
                                case RIGHT:
                                    if (page > 1) {
                                        open(p, clanPlayer, openClan, type, sortMembers, page - 1);
                                    }
                                    break;
                                case LEFT:
                                    int max = openClan.getCollMembers() / 28;
                                    if (openClan.getCollMembers() % 28 > 0)
                                        max++;
                                    if (page < max) {
                                        open(p, clanPlayer, openClan, type, sortMembers, page + 1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        });
                        g.setButton(26, iPage);

                        if (openClan.getCollMembers() > (page - 1) * 24) {
                            int i = (page - 1) * 28;
                            for (int ii = 0; ii < 4; ii++) {
                                for (int jj = 0; jj < 7; jj++) {
                                    if (i < openClan.getMembers().size()) {
                                        g.setButton(35, null);
                                        g.setButton(44, null);
                                        g.setButton(53, null);
                                        ClanPlayers pl = openClan.getMembers().get(i++);
                                        Item_builder bntUpMembers = new Item_builder(upMembers, (s) -> pl.replase(s.replaceFirst("<newRank>", ClanRank.valueOf(pl.getRank().toInt() + 1).toString())))
                                                .ItemListener((item2, click2) -> QPlayerUp.open(p, cp, pl, true, () -> open(p, null, openClan.getId(), type, sortMembers, page)));


                                        Item_builder bntDownMembers = new Item_builder(downMembers,
                                                (s) -> pl.replase(s.replaceFirst("<newRank>", ClanRank.valueOf(pl.getRank().toInt() - 1).toString())))
                                                .ItemListener((item2, click2) -> QPlayerDown.open(p, pl, () -> open(p, null, openClan.getId(), type, sortMembers, page)));

                                        Item_builder bntKickMembers = new Item_builder(kickMembers, pl::replase).ItemListener((item2, click2) -> QPlayerKick.open(p, pl, () -> open(p, null, openClan.getId(), type, sortMembers, page)));

                                        g.setButton((2 + ii) * 9 + jj, pl.getButton(OtherClanMenu.members).amount(pl.getRank().toInt())
                                                .ItemListener((item, click) -> {
                                                    if (p.hasPermission(Pex.PEX_ADMIN_MEMBERS)) {
                                                        ClanRank r = pl.getRank();
                                                        if (r.toInt() < 4) g.setButton(35, bntUpMembers);
                                                        else g.setButton(35, null);
                                                        if (r.toInt() > 1) g.setButton(44, bntDownMembers);
                                                        else g.setButton(44, null);
                                                        g.setButton(53, bntKickMembers);
                                                    }
                                                    g.update();
                                                }));

                                    } else
                                        break;

                                }
                                if (i >= openClan.getCollMembers())
                                    break;
                            }

                        }

                    }
                    break;
                    case admin: {
                        Item_builder fon = new Item_builder(constructor.fonSetting).name(ChatColor.RESET.toString()).localizedName("fon");
                        for (int i = 18; i < 56; i++)
                            g.setButton(i, fon);


                        Item_builder bntClanHome = new Item_builder(clanHome).localizedName("clanHome").ItemListener((ItemStack item, ClickType click) -> {
                            LoadMenu.open(p);
                            switch (click) {
                                case RIGHT:
                                    runAcuns(() -> {
                                        try {
                                            dbManager.setLocation(openClan.getId(), "");
                                            msgMenu.open(p, messages.clearClanHome, () -> open(p, null, openClan.getId(), type, sortMembers, page));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            ErrorMenu.open(p);
                                        }
                                    });
                                    break;
                                default:
                                    Location l = p.getLocation();
                                    StringBuilder loc = new StringBuilder();
                                    loc.append(l.getWorld().getName());
                                    loc.append(":");
                                    loc.append(l.getX());
                                    loc.append(":");
                                    loc.append(l.getY());
                                    loc.append(":");
                                    loc.append(l.getZ());
                                    loc.append(":");
                                    loc.append(l.getPitch());
                                    loc.append(":");
                                    loc.append(l.getYaw());
                                    if (Config.debug)
                                        Main.sendMSGColor(ChatColor.AQUA + "Location: " + loc.toString());

                                    runAcuns(() -> {
                                        try {
                                            dbManager.setLocation(openClan.getId(), loc.toString());
                                            msgMenu.open(p, messages.setClanHome, () -> open(p, null, openClan.getId(), type, sortMembers, page));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            ErrorMenu.open(p);
                                        }
                                    });
                                    break;
                            }
                        });
                        if (openClan.getClanHome() == null) {
                            bntClanHome.setLore(noClanHome);
                        }
                        g.setButton(clanHome.getSlot() + 18, bntClanHome);


                        Item_builder bntAddMembers = new Item_builder(addMembers, openClan::replase).localizedName("addMembers").ItemListener((i, x) -> {
                            adminAddMembers.open(p, clanPlayer, openClan, () -> open(p, null, openClan.getId(), type, sortMembers, page));
                        });
                        g.setButton(addMembers.getSlot() + 18, bntAddMembers);


                        Item_builder bntReName = new Item_builder(reName, openClan::replase).localizedName("reName")
                                .ItemListener((item, click) -> reNameClan.open(p, openClan, () -> open(p, null, openClan.getId(), type, sortMembers, page)));
                        g.setButton(reName.getSlot() + 18, bntReName);

                        if (!Config.nameToTag) {
                            Item_builder bntReTag = new Item_builder(reTag, openClan::replase).localizedName("reTag")
                                    .ItemListener((item, click) -> reNameTag.open(p, openClan, () -> open(p, null, openClan.getId(), type, sortMembers, page)));
                            g.setButton(reTag.getSlot() + 18, bntReTag);
                        }
                        Item_builder bntReIcon = new Item_builder(reIcon, openClan::replase).material(Material.SKULL_ITEM).configData(openClan.getHead()).localizedName("reIcon")
                                .ItemListener((item, click) -> ReIconMenu.open(p, openClan, 1, ReIconMenu.IconSort.name, () -> open(p, null, openClan.getId(), type, sortMembers, page)));
                        g.setButton(reIcon.getSlot() + 18, bntReIcon);

                        Item_builder bntReDescriptions = new Item_builder(reDescriptions, openClan::replase).localizedName("reDescriptions")
                                .ItemListener((item, click) -> {
                                    p.closeInventory();
                                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', messages.reDescriptions));
                                    reDescriptionClan.addReDiscription(p, openClan, () -> open(p, null, openClan.getId(), type, sortMembers, page));
                                });
                        ArrayList<String> lore = new ArrayList<>();
                        for (String s : Objects.requireNonNull(bntReDescriptions.getLore())) {
                            if (s.contains("<descriptions>"))
                                lore.addAll(openClan.getArrayOpis(s));
                            else
                                lore.add(s);
                        }
                        bntReDescriptions.setLore(lore);
                        g.setButton(reDescriptions.getSlot() + 18, bntReDescriptions);

                        Item_builder bntDelClan = new Item_builder(delClan, openClan::replase).localizedName("delClan")
                                .ItemListener((item, click) -> QDisbandClan.open(p, openClan, () -> open(p, null, openClan.getId(), type, sortMembers, page)));
                        g.setButton(delClan.getSlot() + 18, bntDelClan);
                        break;
                    }
                    default:
                        break;
                }

                g.open(p);
            } catch (Exception e) {
                e.printStackTrace();
                Bukkit.getScheduler().runTask(Main.plugin, () -> ErrorMenu.open(p));
            }
        });
    }

}
