package ru.ilyshka_fox.clanfox.setting;

public class Pex {

	public static final String PEX_PLAYER_MENU = 		"clanfox.player.menu";				// Открыть меню
	public static final String PEX_PLAYER_CREATE = 		"clanfox.player.create";			// Создать клан
	public static final String PEX_PLAYER_DELETE = 		"clanfox.player.delete";			// Удалить клан
	public static final String PEX_PLAYER_INVITE =		"clanfox.player.invite";			// Приглашать в клан
	public static final String PEX_CLAN_INVATE_ACCEPT = "clanfox.player.request.buttons";	// Прият/отклонить запрос в клан
	public static final String PEX_CLAN_EDIT_MEMBERS = 	"clanfox.player.members";			// Управление участниками клана
	public static final String PEX_PLAYER_SMS = 		"clanfox.player.mail";				// Сообщение кланк

	public static final String PEX_PLAYER_EXIT = 		"clanfox.player.exit";				// Возможность самостоятельно покинуть клан
	public static final String PEX_PLAYER_INVITE_AD=	"clanfox.player.invite.buttons";	// Принять/отклонить заявку в клан
	public static final String PEX_PLAYER_CHATMODE = 	"clanfox.player.chatmode";  		// Изменять режим чата
	public static final String PEX_PLAYER_OTHERCLAN = 	"clanfox.player.vievclan";			// Просмотреть чужой клан
	public static final String PEX_CLAN_INVITE = 		"clanfox.player.request";			// Подать запрос в клан

	public static final String PEX_PLAYER_RENAME = 		"clanfox.player.rename";			// Переименовать клан
	public static final String PEX_PLAYER_RETEG = 		"clanfox.player.retag";				// Изменить тег клана
	public static final String PEX_PLAYER_ICO = 		"clanfox.player.icon";				// Изменить иконку клана
	public static final String PEX_PLAYER_REOPIS = 		"clanfox.player.redescription";		// Изменить описание клана
	public static final String PEX_PLAYER_CLAN_REOVNER ="clanfox.player.reowner";			// Изменить лидера клана
	public static final String PEX_PLAYER_CLANHOME =	"clanfox.player.setClanHome";		// Установить точку клана

	public static final String PEX_ECO_CREATE = 		"clanfox.eco.create";				// Бесплатно создать клан
	public static final String PEX_ECO_RENAME = 		"clanfox.eco.rename";				// Бесплатно изменить имя клана
	public static final String PEX_ECO_REICO = 			"clanfox.eco.reicon";				// Бесплатно изменить иконку клана
	public static final String PEX_ECO_RETAG = 			"clanfox.eco.retag";				// Бесплатно изменить тег клана
	public static final String PEX_ECO_SETHOME = 		"clanfox.eco.setclanhome";			// Установка точки клана

	public static final String PEX_TIME_DELAY = 		"clanfox.group.maildelay";			// Mail Нет задержки при отправки
	public static final String PEX_GROUP_COLOR = 		"clanfox.group.color";				// Разрешить цветовые коды
	public static final String PEX_GROUP_COLOR_NO_MAX = "clanfox.group.color.delay";		// Нет ограничения по колличеству цветов
	public static final String PEX_GROUP_COLOR_MAGIC = 	"clanfox.group.color.magic";		// Возможность использовать магический цвет
//	public static final String PEX_GROUP_CLAN_UNLIMIT = "clanfox.group.clan.unlimit";		// Без размерный клан

	public static final String PEX_ADMIN_CLAN = 		"clanfox.admin.clan";				// Возможность изменять настрокий клана
	public static final String PEX_ADMIN_MEMBERS = 		"clanfox.admin.member";				// Возможность изменять ранг игроков
	public static final String PEX_ADMIN_MENU = 		"clanfox.admin.reload";				// Возможность перезагрузить плагин
	public static final String PEX_ADMIN_CHAT= 			"clanfox.admin.chat";				// Просмотр клановых чатов


}
